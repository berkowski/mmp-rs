use mmp_derive::{Deserialize, Serialize};
//use mmp_serialize::{de::Deserialize, ser::Serialize};

#[repr(u8)]
enum Subsystem {
    Example = 5,
}

impl Into<u8> for Subsystem {
    fn into(self) -> u8 {
        self as u8
    }
}

#[repr(u8)]
enum Field {
    ExampleField = 3,
}

impl Into<u8> for Field {
    fn into(self) -> u8 {
        self as u8
    }
}

struct U32(u32);

impl<'a> mmp_serialize::ser::Serialize<'a> for U32 {
    fn serialize(&self, serializer: mmp_serialize::ser::Serializer<'a>) -> Result<mmp_serialize::ser::Serializer<'a>, mmp_serialize::Error> {
        serializer.serialize_u32(self.0)
    }
}

impl<'de> mmp_serialize::de::Deserialize<'de> for U32 {
    fn deserialize(deserializer: mmp_serialize::de::Deserializer<'de>) -> Result<(mmp_serialize::de::Deserializer<'de>, Self), mmp_serialize::Error> {
        let (deserializer, value) = deserializer.deserialize_u32()?;
        Ok((deserializer, Self(value)))
    }
}

impl From<u16> for U32 {
    fn from(v: u16) -> Self {
        Self(v as u32)
    }
}

impl From<U32> for u16 {
    fn from(v: U32) -> Self {
        v.0 as Self
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[mmp(system = "Subsystem::Example")]
#[mmp(field = "Field::ExampleField")]
#[mmp(kind = "U32")]
pub struct Example(pub u16);

fn main() {
    let value = Example(0x55);
    let mut buf = [0u8; 16];

    let expected = &[Subsystem::Example.into(), Field::ExampleField.into(), 0x00, 0x04, 0x00, 0x00, 0x00, 0x55][..];
    let (result, _tail) = mmp_serialize::to_bytes(&value, &mut buf[..]).unwrap();
    assert_eq!(expected, result);

    let (_, value2) = mmp_serialize::from_bytes(expected).unwrap();
    assert_eq!(value, value2);
}
