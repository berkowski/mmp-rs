use mmp_derive::{Deserialize, Serialize};
//use mmp_serialize::{de::Deserialize, ser::Serialize};

#[repr(u8)]
enum Subsystem {
    Example = 5,
}

impl Into<u8> for Subsystem {
    fn into(self) -> u8 {
        self as u8
    }
}

#[repr(u8)]
enum Field {
    ExampleField = 3,
}

impl Into<u8> for Field {
    fn into(self) -> u8 {
        self as u8
    }
}

#[derive(Serialize, Deserialize)]
#[mmp(system = "Subsystem::Example")]
#[mmp(field = "Field::ExampleField")]
pub struct Example {
    int_val: i16,
    bool_val: bool,
}

fn main() {}