use mmp_derive::{Deserialize, Serialize};
//use mmp_serialize::{de::Deserialize, ser::Serialize};

#[derive(Serialize, Deserialize)]
#[repr(u8)]
pub enum ExampleEnum {
    One = 1,
    Two = 2,
}

impl From<ExampleEnum> for u8 {
    fn from(v: ExampleEnum) -> Self {
        match v {
            ExampleEnum::One => 1,
            ExampleEnum::Two => 2,
        }
    }
}

impl ::core::convert::TryInto<ExampleEnum> for u8 {
    type Error = ();

    fn try_into(self) -> Result<ExampleEnum, ()> {
        match self {
            1 => Ok(ExampleEnum::One),
            2 => Ok(ExampleEnum::Two),
            _ => Err(()),
        }
    }
}

fn main() {}
