#[test]
fn tests() {
    let t = trybuild::TestCases::new();
    t.pass("tests/derive_enum.rs");
    t.pass("tests/derive_struct.rs");
    t.pass("tests/derive_tuple_struct.rs");
}
