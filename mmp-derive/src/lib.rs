use proc_macro::{self, TokenStream};
use quote::quote;
use syn::{Data, DataEnum, DeriveInput, Ident, LitStr};

#[derive(Debug)]
struct Attributes {
    pub enum_repr: Option<Ident>,
    pub system: Option<syn::LitStr>,
    pub field: Option<syn::LitStr>,
    pub kind: Option<syn::LitStr>,
}

impl TryFrom<Vec<syn::Attribute>> for Attributes {
    type Error = syn::parse::Error;

    fn try_from(attrs: Vec<syn::Attribute>) -> Result<Self, Self::Error> {
        let mut enum_repr = None;
        let mut system = None;
        let mut field = None;
        let mut kind = None;

        for attr in attrs.iter() {
            if attr.path().is_ident("repr") {
                attr.parse_nested_meta(|meta| {
                    if meta.path.is_ident("C") {
                        Err(meta.error("repr(C) does not have a well defined size"))
                    } else if enum_repr.is_some() {
                        Err(meta.error("found multiple repr(INTEGER) values"))
                    } else {
                        let ident = meta.path.require_ident()?;
                        enum_repr.replace(ident.clone());
                        Ok(())
                    }
                })?;
            }
            if attr.path().is_ident("mmp") {
                attr.parse_nested_meta(|meta| {
                    if meta.path.is_ident("kind") {
                        let value = meta.value()?;
                        let s: LitStr = value.parse()?;
                        kind.replace(s);
                        Ok(())
                    } else if meta.path.is_ident("field") {
                        let value = meta.value()?;
                        let s: LitStr = value.parse()?;
                        field.replace(s);
                        Ok(())
                    } else if meta.path.is_ident("system") {
                        let value = meta.value()?;
                        let s: LitStr = value.parse()?;
                        system.replace(s);
                        Ok(())
                    } else {
                        Err(meta.error("unsupported attribute"))
                    }
                })?;
            }
        }

        Ok(Self {
            enum_repr,
            field,
            system,
            kind,
        })
    }
}

// parse_lit_str, spanned_tokens, respan, and respan_token taken from the serde_derive crate
// All combined, takes a string value and creates a syn::ExprPath.
// Used for reading the "system" and "field" macro attributes for (De)Serialization.
fn parse_lit_str<T>(s: &syn::LitStr) -> syn::parse::Result<T>
where
    T: syn::parse::Parse,
{
    let tokens = spanned_tokens(s)?;
    syn::parse2(tokens)
}

fn spanned_tokens(s: &syn::LitStr) -> syn::parse::Result<proc_macro2::TokenStream> {
    let stream = syn::parse_str(&s.value())?;
    Ok(respan(stream, s.span()))
}

fn respan(stream: proc_macro2::TokenStream, span: proc_macro2::Span) -> proc_macro2::TokenStream {
    stream
        .into_iter()
        .map(|token| respan_token(token, span))
        .collect()
}

fn respan_token(
    mut token: proc_macro2::TokenTree,
    span: proc_macro2::Span,
) -> proc_macro2::TokenTree {
    if let proc_macro2::TokenTree::Group(g) = &mut token {
        *g = proc_macro2::Group::new(g.delimiter(), respan(g.stream(), span));
    }
    token.set_span(span);
    token
}

#[proc_macro_derive(Serialize, attributes(mmp))]
pub fn serialize(input: TokenStream) -> TokenStream {
    let DeriveInput {
        ident, data, attrs, ..
    } = syn::parse_macro_input! {input};

    let attributes = Attributes::try_from(attrs).expect("Unable to convert attributes.");

    match data {
        Data::Struct(_struct) => {
            let field = attributes
                .field
                .expect("Missing #[mmp(field = \"..\")] attribute");
            let system = attributes
                .system
                .expect("Missing #[mmp(system = \"..\")] attribute");
            match _struct.fields {
                syn::Fields::Named(fields) => {
                    pack_struct_with_named_fields(ident, system, field, fields)
                }
                syn::Fields::Unnamed(fields) => {
                    let kind = attributes
                        .kind
                        .expect("Missing #[mmp(kind = \"..\")] attribute");

                    if fields.unnamed.len() != 1 {
                        panic!(
                            "[#derive(Serialize) only available for  tuple structs with one field"
                        );
                    }

                    pack_struct_with_unnamed_fields(ident, system, field, kind)
                }
                syn::Fields::Unit => panic!("#[derive(Serialize)] not usuable on unit structs"),
            }
        }
        Data::Enum(_enum) => {
            let repr = attributes
                .enum_repr
                .expect("Missing `#[repr(Integer)] attribute");
            pack_enum(ident, repr, _enum)
        }
        Data::Union(_) => {
            panic!("#[derive(Serialize)] not usable on Union types");
        }
    }
}

fn pack_struct_with_named_fields(
    ident: Ident,
    system: syn::LitStr,
    field: syn::LitStr,
    fields: syn::FieldsNamed,
) -> TokenStream {
    let (name, ty): (Vec<syn::Ident>, Vec<syn::Type>) = fields
        .named
        .into_pairs()
        .map(|p| p.into_value())
        .map(|v| (v.ident.unwrap(), v.ty))
        .unzip();

    let system: syn::ExprPath = parse_lit_str(&system).unwrap();
    let field: syn::ExprPath = parse_lit_str(&field).unwrap();

    quote! {
        impl<'a> ::mmp_serialize::ser::Serialize<'a> for #ident {
            fn serialize(&self, mut serializer: ::mmp_serialize::ser::Serializer<'a>) -> Result<::mmp_serialize::ser::Serializer<'a>, ::mmp_serialize::ser::Error> {
                let system: u8 = #system.into();
                let field: u8 = #field.into();
                let serializer = system.serialize(serializer)?;
                let serializer = field.serialize(serializer)?;
                let serializer = serializer.start_length()?;
                #(
                    let serializer = <#ty as ::mmp_serialize::ser::Serialize>::serialize(&self.#name, serializer)?;
                )*
                serializer.finish_length()
            }
        }
    }.into()
}

fn pack_struct_with_unnamed_fields(
    ident: Ident,
    system: syn::LitStr,
    field: syn::LitStr,
    kind: syn::LitStr,
) -> TokenStream {
    let system: syn::ExprPath = parse_lit_str(&system).unwrap();
    let field: syn::ExprPath = parse_lit_str(&field).unwrap();
    let kind: syn::ExprPath = parse_lit_str(&kind).unwrap();

    quote! {
        impl<'a> ::mmp_serialize::ser::Serialize<'a> for #ident {
            fn serialize(&self, mut serializer: ::mmp_serialize::ser::Serializer<'a>) -> Result<::mmp_serialize::ser::Serializer<'a>, ::mmp_serialize::ser::Error> {
                let system: u8 = #system.into();
                let field: u8 = #field.into();
                let serializer = system.serialize(serializer)?;
                let serializer = field.serialize(serializer)?;
                let serializer = serializer.start_length()?;
                let value = #kind::from(self.0);
                let serializer = <#kind as ::mmp_serialize::ser::Serialize>::serialize(&value, serializer)?;
                serializer.finish_length()
            }
        }
    }.into()
}

fn pack_enum(ident: Ident, repr: Ident, _enum: DataEnum) -> TokenStream {
    let (variant, _expr): (Vec<syn::Ident>, Vec<syn::Expr>) = _enum
        .variants
        .into_pairs()
        .map(|p| p.into_value())
        .map(|v| {
            let ident = v.ident;
            let discriminant =
                v.discriminant.expect(&format!(
                "#[derive(Serializer)] on enum's requires explicit discriminants (invalid variant: {})",
                ident));
            (ident, discriminant.1)
        })
        .unzip();

    quote! {
        impl<'a> ::mmp_serialize::ser::Serialize<'a> for #ident {
            fn serialize(&self, serializer: ::mmp_serialize::ser::Serializer<'a>) -> Result<::mmp_serialize::ser::Serializer<'a>, ::mmp_serialize::ser::Error> {
                let serializer = match self {
                    #(
                        Self::#variant => <#repr as ::mmp_serialize::ser::Serialize>::serialize(&Self::#variant.into(), serializer),
                    )*
                }?;
                Ok(serializer)
            }
        }
    }.into()
}

#[proc_macro_derive(Deserialize, attributes(mmp))]
pub fn deserialize(input: TokenStream) -> TokenStream {
    let DeriveInput {
        ident, data, attrs, ..
    } = syn::parse_macro_input! {input};

    let attributes = Attributes::try_from(attrs).expect("unable to convert all attributes");

    match data {
        Data::Struct(_struct) => {
            let field = attributes
                .field
                .expect("Missing #[mmp(field = \"..\")] attribute");
            let system = attributes
                .system
                .expect("Missing #[mmp(system = \"..\")] attribute");
            match _struct.fields {
                syn::Fields::Named(fields) => {
                    unpack_struct_with_named_fields(ident, system, field, fields)
                }
                syn::Fields::Unnamed(fields) => {
                    let kind = attributes
                        .kind
                        .expect("Missing #[mmp(kind = \"..\")] attribute");

                    if fields.unnamed.len() != 1 {
                        panic!("[#derive(Deserialize) only available for  tuple structs with one field");
                    }

                    unpack_struct_with_unnamed_fields(ident, system, field, kind)
                }
                syn::Fields::Unit => panic!("#[derive(Deserialize)] not usuable on unit structs"),
            }
        }
        Data::Enum(_enum) => {
            let repr = attributes
                .enum_repr
                .expect("Missing `#[repr(Integer)] attribute");
            unpack_enum(ident, repr, _enum)
        }
        Data::Union(_) => {
            panic!("#[derive(Deserialize)] not usable on Union types");
        }
    }
}

fn unpack_struct_with_named_fields(
    ident: Ident,
    system: syn::LitStr,
    field: syn::LitStr,
    fields: syn::FieldsNamed,
) -> TokenStream {
    let (name, _ty): (Vec<syn::Ident>, Vec<syn::Type>) = fields
        .named
        .into_pairs()
        .map(|p| p.into_value())
        .map(|v| (v.ident.unwrap(), v.ty))
        .unzip();

    let system: syn::ExprPath = parse_lit_str(&system).unwrap();
    let field: syn::ExprPath = parse_lit_str(&field).unwrap();

    quote! {
        impl<'de> ::mmp_serialize::de::Deserialize<'de> for #ident {
            fn deserialize(mut deserializer: ::mmp_serialize::de::Deserializer<'de>) -> Result<(::mmp_serialize::de::Deserializer<'de>, Self), ::mmp_serialize::de::Error> {
                let expected_system: u8 = #system.into();
                let expected_field: u8 = #field.into();
                let (deserializer, system) = deserializer.deserialize_u8()?;
                if system != #system.into() {
                    return Err(::mmp_serialize::de::Error::SubsystemMismatch);
                }
                let (deserializer, field) = deserializer.deserialize_u8()?;
                    if field != #field.into() {
                return Err(::mmp_serialize::de::Error::FieldMismatch);
                }

                let (deserializer, _len) = deserializer.deserialize_u16()?;

                #(
                    let (deserializer, #name) = ::mmp_serialize::de::Deserialize::deserialize(deserializer)?;
                )*
                Ok((deserializer, #ident{#(#name,)*}))
            }
        }
    }.into()
}

fn unpack_struct_with_unnamed_fields(
    ident: Ident,
    system: syn::LitStr,
    field: syn::LitStr,
    kind: syn::LitStr,
) -> TokenStream {
    let system: syn::ExprPath = parse_lit_str(&system).unwrap();
    let field: syn::ExprPath = parse_lit_str(&field).unwrap();
    let kind: syn::ExprPath = parse_lit_str(&kind).unwrap();

    quote! {
        impl<'de> ::mmp_serialize::de::Deserialize<'de> for #ident {
            fn deserialize(mut deserializer: ::mmp_serialize::de::Deserializer<'de>) -> Result<(::mmp_serialize::de::Deserializer<'de>, Self), ::mmp_serialize::de::Error> {
                let expected_system: u8 = #system.into();
                let expected_field: u8 = #field.into();
                let (deserializer, system) = deserializer.deserialize_u8()?;
                if system != #system.into() {
                    return Err(::mmp_serialize::de::Error::SubsystemMismatch);
                }
                let (deserializer, field) = deserializer.deserialize_u8()?;
                if field != #field.into() {
                    return Err(::mmp_serialize::de::Error::FieldMismatch);
                }

                let (deserializer, _len) = deserializer.deserialize_u16()?;

                let (deserializer, value) = <#kind as ::mmp_serialize::de::Deserialize>::deserialize(deserializer)?;
                Ok((deserializer, #ident(value.into())))
            }
        }
    }.into()
}

fn unpack_enum(ident: Ident, repr: Ident, _enum: DataEnum) -> TokenStream {
    quote! {
        impl<'de> ::mmp_serialize::de::Deserialize<'de> for #ident {
            fn deserialize(deserializer: ::mmp_serialize::de::Deserializer<'de>) -> Result<(::mmp_serialize::de::Deserializer<'de>, Self), ::mmp_serialize::de::Error> {
                let (deserializer, value): (_, #repr) = ::mmp_serialize::de::Deserialize::deserialize(deserializer)?;
                let value = ::core::convert::TryInto::try_into(value).map_err(|_| ::mmp_serialize::de::Error::Deserialize)?;
                Ok((deserializer, value))
            }
        }
    }.into()
}
