* 0.2   [2023-03-01]
** CHANGED
- Bumped `syn` to version 2

* 0.1 [UNRELEASED]
Initial release

