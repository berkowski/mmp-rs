use clap::Parser;
use log::info;

use mmp_serialize::{Error, Serialize};
use std::io::Read;
use std::time::Duration;

#[derive(Debug, Parser)]
#[clap(author, version, about)]
struct Opt {
    /// Serial port path
    #[clap(short, long, value_parser, default_value_t = String::from("/dev/ttyUSB0"))]
    port: String,
    #[clap(short, long, value_parser, default_value_t = 9600)]
    baud: u32,
}

fn main() -> Result<(), Error> {
    env_logger::Builder::new()
        .filter_level(log::LevelFilter::Info)
        .init();

    let args = Opt::parse();

    let mut port = serialport::new(&args.port, args.baud)
        .timeout(Duration::from_secs(1))
        .open()
        .unwrap();
    info!("Opened serial port: {} @{}bps, 8N1", &args.port, args.baud);

    let mut buffer = [0u8; 1024];
    let (header, payload) = buffer.split_at_mut(4);
    header[0] = mmp::MMP_SENTINEL;
    header[1] = 1;
    header[2] = mmp::MessageKind::Set.into();

    let serializer = mmp_serialize::ser::Serializer::with_slice(payload);

    // Unblock all notifications
    let serializer = mmp::iface::BlockNotify { subsys: &[] }.serialize(serializer)?;
    header[3] += 1;

    let all_subsystems = mmp::Subsystem::iter_fields()
        .copied()
        .map(|f| f.into())
        .collect::<Vec<u8>>();

    let serializer = mmp::iface::UnblockNotify {
        subsys: all_subsystems.as_slice(),
    }
    .serialize(serializer)?;

    header[3] += 1;

    let (payload, _) = serializer.finish();
    let size = payload.len() + header.len();
    let message = &buffer[..size];

    port.write_all(&message).unwrap();
    port.flush().unwrap();
    info!("Set Notification Message: {:02x?}", &message);

    let (header, payload) = buffer.split_at_mut(4);

    header[2] = mmp::MessageKind::Get.into();
    header[3] = 0;

    let i = 4 * (header[3] as usize);
    payload[i..i + 2].copy_from_slice(&[
        mmp::Subsystem::ConfigModem.into(),
        mmp::modem::Field::LocalAddr.into(),
    ]);
    header[3] += 1;

    let i = 4 * (header[3] as usize);
    payload[i..i + 2].copy_from_slice(&[
        mmp::Subsystem::ConfigXpnd.into(),
        mmp::xpnd::Field::RxFreq.into(),
    ]);
    header[3] += 1;

    let i = 4 * (header[3] as usize);
    payload[i..i + 2].copy_from_slice(&[
        mmp::Subsystem::ConfigXpnd.into(),
        mmp::xpnd::Field::ResponseFreq.into(),
    ]);
    header[3] += 1;

    let i = 4 * (header[3] as usize);
    payload[i..i + 2].copy_from_slice(&[
        mmp::Subsystem::ConfigSystem.into(),
        mmp::system::Field::RlsType.into(),
    ]);
    header[3] += 1;

    let i = 4 * (header[3] as usize);
    payload[i..i + 2].copy_from_slice(&[
        mmp::Subsystem::ConfigRelease.into(),
        mmp::release::Field::RlsCode.into(),
    ]);
    header[3] += 1;

    let size = (4 * header[3] + 4) as usize;
    let message = &buffer[..size];

    port.write_all(&message).unwrap();
    port.flush().unwrap();
    info!("Get Parameters Message: {:02x?}", &message);

    let mut mmp_reader = mmp::reader::MmpReader::<128>::new();
    for b in port.bytes().flatten() {
        if let Ok(Some(param)) = mmp_reader.consume_byte(b) {
            info!("{:02X?}", param);
        }
    }
    Ok(())
}
