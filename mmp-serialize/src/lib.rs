#![no_std]

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Error {
    Full,
    UnexpectedEOF,
    Deserialize,
    SubsystemMismatch,
    FieldMismatch,
}

pub mod ser {
    pub use super::Error;

    pub struct Serializer<'a> {
        buffer: &'a mut [u8],
        index: usize,
        marker: Option<usize>,
    }

    impl<'a> Serializer<'a> {
        pub fn with_slice(buffer: &'a mut [u8]) -> Self {
            Self {
                buffer,
                index: 0,
                marker: None,
            }
        }

        pub fn finish(self) -> (&'a [u8], &'a mut [u8]) {
            let (head, tail) = self.buffer.split_at_mut(self.index);
            (head, tail)
        }

        pub fn start_length(mut self) -> Result<Self, Error> {
            if self.buffer.len() - self.index < 2 {
                Err(Error::Full)
            } else {
                self.marker.replace(self.index);
                self.index += 2;
                Ok(self)
            }
        }

        pub fn finish_length(mut self) -> Result<Self, Error> {
            if let Some(start) = self.marker {
                if start < self.index - 2 {
                    let len: u16 = (self.index - start - 2) as u16;
                    self.buffer[start..start + 2].copy_from_slice(&len.to_be_bytes());
                    self.marker = None;
                }
            }
            Ok(self)
        }

        fn pack(&mut self, val: &[u8]) -> Result<(), Error> {
            let len = val.len();
            if len <= self.buffer.len() - self.index {
                unsafe { self.pack_unchecked(val, len) };
                Ok(())
            } else {
                Err(Error::Full)
            }
        }

        unsafe fn pack_unchecked(&mut self, val: &[u8], len: usize) {
            self.buffer[self.index..self.index + len].copy_from_slice(val);
            self.index += len;
        }

        pub fn serialize_u8(mut self, v: u8) -> Result<Self, Error> {
            self.pack(&v.to_be_bytes()[..])?;
            Ok(self)
        }

        pub fn serialize_i8(mut self, v: i8) -> Result<Self, Error> {
            self.pack(&v.to_be_bytes()[..])?;
            Ok(self)
        }

        pub fn serialize_u16(mut self, v: u16) -> Result<Self, Error> {
            self.pack(&v.to_be_bytes()[..])?;
            Ok(self)
        }

        pub fn serialize_i16(mut self, v: i16) -> Result<Self, Error> {
            self.pack(&v.to_be_bytes()[..])?;
            Ok(self)
        }
        pub fn serialize_u32(mut self, v: u32) -> Result<Self, Error> {
            self.pack(&v.to_be_bytes()[..])?;
            Ok(self)
        }

        pub fn serialize_i32(mut self, v: i32) -> Result<Self, Error> {
            self.pack(&v.to_be_bytes()[..])?;
            Ok(self)
        }
        pub fn serialize_u64(mut self, v: u64) -> Result<Self, Error> {
            self.pack(&v.to_be_bytes()[..])?;
            Ok(self)
        }

        pub fn serialize_i64(mut self, v: i64) -> Result<Self, Error> {
            self.pack(&v.to_be_bytes()[..])?;
            Ok(self)
        }

        pub fn serialize_bool(mut self, v: bool) -> Result<Self, Error> {
            self.pack(&(v as u8).to_be_bytes()[..])?;
            Ok(self)
        }

        pub fn serialize_bytes(mut self, v: &[u8]) -> Result<Self, Error> {
            self.pack(v)?;
            Ok(self)
        }
    }

    pub trait Serialize<'a> {
        fn serialize(&self, serializer: Serializer<'a>) -> Result<Serializer<'a>, Error>;
    }

    impl<'a> Serialize<'a> for u8 {
        fn serialize(&self, serializer: Serializer<'a>) -> Result<Serializer<'a>, Error> {
            serializer.serialize_u8(*self)
        }
    }

    impl<'a> Serialize<'a> for i8 {
        fn serialize(&self, serializer: Serializer<'a>) -> Result<Serializer<'a>, Error> {
            serializer.serialize_i8(*self)
        }
    }
    impl<'a> Serialize<'a> for u16 {
        fn serialize(&self, serializer: Serializer<'a>) -> Result<Serializer<'a>, Error> {
            serializer.serialize_u16(*self)
        }
    }

    impl<'a> Serialize<'a> for i16 {
        fn serialize(&self, serializer: Serializer<'a>) -> Result<Serializer<'a>, Error> {
            serializer.serialize_i16(*self)
        }
    }
    impl<'a> Serialize<'a> for u32 {
        fn serialize(&self, serializer: Serializer<'a>) -> Result<Serializer<'a>, Error> {
            serializer.serialize_u32(*self)
        }
    }

    impl<'a> Serialize<'a> for i32 {
        fn serialize(&self, serializer: Serializer<'a>) -> Result<Serializer<'a>, Error> {
            serializer.serialize_i32(*self)
        }
    }
    impl<'a> Serialize<'a> for u64 {
        fn serialize(&self, serializer: Serializer<'a>) -> Result<Serializer<'a>, Error> {
            serializer.serialize_u64(*self)
        }
    }

    impl<'a> Serialize<'a> for i64 {
        fn serialize(&self, serializer: Serializer<'a>) -> Result<Serializer<'a>, Error> {
            serializer.serialize_i64(*self)
        }
    }
    impl<'a> Serialize<'a> for bool {
        fn serialize(&self, serializer: Serializer<'a>) -> Result<Serializer<'a>, Error> {
            serializer.serialize_bool(*self)
        }
    }

    impl<'a> Serialize<'a> for &[u8] {
        fn serialize(&self, serializer: Serializer<'a>) -> Result<Serializer<'a>, Error> {
            serializer.serialize_bytes(*self)
        }
    }
}

pub mod de {
    pub use super::Error;

    pub struct Deserializer<'a> {
        buffer: &'a [u8],
        index: usize,
    }

    impl<'a> Deserializer<'a> {
        pub fn with_slice(buffer: &'a [u8]) -> Self {
            Self { buffer, index: 0 }
        }

        pub fn finish(self) -> &'a [u8] {
            &self.buffer[self.index..]
        }

        fn split<const N: usize>(&mut self) -> Result<[u8; N], Error> {
            if N <= self.buffer.len() - self.index {
                Ok(unsafe { self.split_unchecked::<N>() })
            } else {
                Err(Error::UnexpectedEOF)
            }
        }

        unsafe fn split_unchecked<const N: usize>(&mut self) -> [u8; N] {
            let mut bytes = [0u8; N];
            bytes.copy_from_slice(&self.buffer[self.index..self.index + N]);
            self.index += N;
            bytes
        }

        pub fn deserialize_u8(mut self) -> Result<(Self, u8), Error> {
            let v: u8 = u8::from_be_bytes(self.split::<1>()?);
            Ok((self, v))
        }

        pub fn deserialize_i8(mut self) -> Result<(Self, i8), Error> {
            let v: i8 = i8::from_be_bytes(self.split::<1>()?);
            Ok((self, v))
        }

        pub fn deserialize_u16(mut self) -> Result<(Self, u16), Error> {
            let v: u16 = u16::from_be_bytes(self.split::<2>()?);
            Ok((self, v))
        }

        pub fn deserialize_i16(mut self) -> Result<(Self, i16), Error> {
            let v: i16 = i16::from_be_bytes(self.split::<2>()?);
            Ok((self, v))
        }

        pub fn deserialize_u32(mut self) -> Result<(Self, u32), Error> {
            let v: u32 = u32::from_be_bytes(self.split::<4>()?);
            Ok((self, v))
        }

        pub fn deserialize_i32(mut self) -> Result<(Self, i32), Error> {
            let v: i32 = i32::from_be_bytes(self.split::<4>()?);
            Ok((self, v))
        }

        pub fn deserialize_u64(mut self) -> Result<(Self, u64), Error> {
            let v: u64 = u64::from_be_bytes(self.split::<8>()?);
            Ok((self, v))
        }

        pub fn deserialize_i64(mut self) -> Result<(Self, i64), Error> {
            let v: i64 = i64::from_be_bytes(self.split::<8>()?);
            Ok((self, v))
        }

        pub fn deserialize_bool(mut self) -> Result<(Self, bool), Error> {
            let v = u8::from_be_bytes(self.split::<1>()?) != 0;
            Ok((self, v))
        }

        // pub fn deserialize_bytes<const N: usize>(mut self) -> Result<(Self, [u8; N]), Error> {
        //     if N <= self.buffer[self.index..].len() {
        //         let mut v = [0u8; N];
        //         v.copy_from_slice(&self.buffer[self.index..self.index + N]);
        //         self.index += N;
        //         Ok((self, v))
        //     } else {
        //         Err(Error::UnexpectedEOF)
        //     }
        // }
        pub fn deserialize_bytes(mut self, len: usize) -> Result<(Self, &'a [u8]), Error> {
            if len <= self.buffer[self.index..].len() {
                let v = &self.buffer[self.index..self.index + len];
                self.index += len;
                Ok((self, v))
            } else {
                Err(Error::UnexpectedEOF)
            }
        }
    }

    pub trait Deserialize<'de>: Sized {
        fn deserialize(deserializer: Deserializer<'de>)
            -> Result<(Deserializer<'de>, Self), Error>;
    }

    impl<'de> Deserialize<'de> for u8 {
        fn deserialize(
            deserializer: Deserializer<'de>,
        ) -> Result<(Deserializer<'de>, Self), Error> {
            deserializer.deserialize_u8()
        }
    }
    impl<'de> Deserialize<'de> for i8 {
        fn deserialize(
            deserializer: Deserializer<'de>,
        ) -> Result<(Deserializer<'de>, Self), Error> {
            deserializer.deserialize_i8()
        }
    }
    impl<'de> Deserialize<'de> for u16 {
        fn deserialize(
            deserializer: Deserializer<'de>,
        ) -> Result<(Deserializer<'de>, Self), Error> {
            deserializer.deserialize_u16()
        }
    }
    impl<'de> Deserialize<'de> for i16 {
        fn deserialize(
            deserializer: Deserializer<'de>,
        ) -> Result<(Deserializer<'de>, Self), Error> {
            deserializer.deserialize_i16()
        }
    }
    impl<'de> Deserialize<'de> for u32 {
        fn deserialize(
            deserializer: Deserializer<'de>,
        ) -> Result<(Deserializer<'de>, Self), Error> {
            deserializer.deserialize_u32()
        }
    }
    impl<'de> Deserialize<'de> for i32 {
        fn deserialize(
            deserializer: Deserializer<'de>,
        ) -> Result<(Deserializer<'de>, Self), Error> {
            deserializer.deserialize_i32()
        }
    }
    impl<'de> Deserialize<'de> for u64 {
        fn deserialize(
            deserializer: Deserializer<'de>,
        ) -> Result<(Deserializer<'de>, Self), Error> {
            deserializer.deserialize_u64()
        }
    }
    impl<'de> Deserialize<'de> for i64 {
        fn deserialize(
            deserializer: Deserializer<'de>,
        ) -> Result<(Deserializer<'de>, Self), Error> {
            deserializer.deserialize_i64()
        }
    }
    impl<'de> Deserialize<'de> for bool {
        fn deserialize(
            deserializer: Deserializer<'de>,
        ) -> Result<(Deserializer<'de>, Self), Error> {
            deserializer.deserialize_bool()
        }
    }
}

pub use de::Deserialize;
pub use ser::Serialize;

pub fn from_bytes<'de, D: de::Deserialize<'de>>(bytes: &'de [u8]) -> Result<(&'de [u8], D), Error> {
    let (deserializer, v) = D::deserialize(de::Deserializer::with_slice(bytes))?;
    Ok((deserializer.finish(), v))
}

pub fn to_bytes<'a, T: ser::Serialize<'a>>(
    v: &T,
    bytes: &'a mut [u8],
) -> Result<(&'a [u8], &'a mut [u8]), Error> {
    let serializer = v.serialize(ser::Serializer::with_slice(bytes))?;
    Ok(serializer.finish())
}

pub use mmp_derive::{Deserialize, Serialize};
