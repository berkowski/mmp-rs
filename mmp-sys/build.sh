bindgen --ctypes-prefix=cty --use-core --default-enum-style moduleconsts \
  --generate "types,vars" --no-layout-tests \
  --blacklist-type "va_list" \
  --blacklist-type "__vcrt_bool" \
  --blacklist-type "wchar_t" \
  --blacklist-type "size_t" \
  --blacklist-item "__security_cookie" \
  -o src/generated_bindings.rs wrapper.h -- -fpack-struct
bindgen --ctypes-prefix=cty --use-core --default-enum-style moduleconsts --generate "types,vars" --no-layout-tests --blacklist-type "va_list" --blacklist-type "__vcrt_bool" --blacklist-type "wchar_t"  --blacklist-type "size_t" --blacklist-item "__security_cookie" -o src/generated_bindings.rs wrapper.h -- -fpack-struct
