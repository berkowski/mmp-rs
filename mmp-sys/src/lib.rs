#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![no_std]

pub mod ctypes {
    pub type c_char = i8;
    pub type c_schar = i8;
    pub type c_uchar = u8;
    pub type c_ulonglong = u64;
    pub type c_longlong = i64;
    pub type c_ushort = u16;
    pub type c_short = i16;
    pub type c_int = i32;
    pub type c_uint = u32;
}

#[repr(C, packed)]
#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct mmp_cfg_paramval_t<'a> {
    pub val_or_len: i32,
    pub places: u8,
    pub type_: u8,
    pub buf: &'a [u8],
}

// Not present in mmp_c_defs.h
#[repr(C, packed)]
#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct mmp_gps_relay_t {
    pub status: u16,
}

include!("bindings.rs");
