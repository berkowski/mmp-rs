* mmpd-sys: Bindings for Types and Variables for the Benthos Modem Management Protocol

These bindings are generated from the C API provided by Benthos defined in "mmp_c_defs.h".
The bindings are generated using the bindgen cli program instead of within a `build.rs`
as this is a serialization/deserialization API for talking with a remote instrument without
any FFI calls or linkage required.


* Generating new bindings
The MMP API only applies to data structures.  There are no libraries
to link against or functions to wrap.  Therefore there is no advantage
of using ~bindgen~ in a ~build.rs~ build script.  In fact there are
real disadvantages - ~bindgen~ becomes a dependency and much be run on
all targets building the package.  There's no reason to require this.

To generate an updated set of bindings, use the provided ~bindgen.sh~
shell script.
