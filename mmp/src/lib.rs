#![no_std]
// #![recursion_limit = "256"]

pub mod analog;
//pub mod builder;
pub mod config;
pub mod coproc;
pub mod data;
pub mod datalog;
pub mod deckbox;
pub mod dpsk;
//mod helper;
pub mod iface;
pub mod modem;
pub mod nav;
pub mod pressure;
pub mod reader;
pub mod recorder;
pub mod release;
pub mod rngrls;
pub mod sdk;
pub mod serial;
pub mod sonar;
pub mod sreg;
pub mod system;
pub mod test;
pub mod time;
pub mod transpond;
pub mod transport;
pub mod version;
pub mod whoi;
pub mod xpnd;

#[allow(unused_imports)]
pub(crate) mod log {
    cfg_if::cfg_if! {
        if #[cfg(feature = "log")] {
            pub(crate) use ::log::{debug, error, info, trace, warn};
        }
        else if #[cfg(feature = "defmt")] {
            pub(crate) use ::defmt::{debug, error, info, trace, warn};
        }
        else {
            macro_rules! trace {
                ($s:literal $(, $x:expr)* $(,)?) => {{}};
            }

            #[allow(unused_imports)]
            pub(crate) use trace;

            macro_rules! _debug {
                ($s:literal $(, $x:expr)* $(,)?) => {{}};
            }

            pub(crate) use _debug as debug;

            macro_rules! _info {
                ($s:literal $(, $x:expr)* $(,)?) => {{}};
            }

            pub(crate) use _info as info;

            macro_rules! _warn {
                ($s:literal $(, $x:expr)* $(,)?) => {{}};
            }

            pub(crate) use _warn as warn;

            macro_rules! _error {
                ($s:literal $(, $x:expr)* $(,)?) => {{}};
            }

            pub(crate) use _error as error;
        }
    }
}

use num_enum::{IntoPrimitive, TryFromPrimitive};

/// Kind of MMP Message
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
pub enum MessageKind {
    /// Indicates an MMP GET operation
    Get = mmp_sys::MMP_GET,
    /// Indicates an MMP SET operation
    Set = mmp_sys::MMP_SET,
    /// Indicates an MMP NOTIFY operation
    Notify = mmp_sys::MMP_NOTIFY,
    /// Indicates an MMP EXECUTE operation
    Execute = mmp_sys::MMP_EXECUTE,
}

/// MMP sentinal byte
pub const MMP_SENTINEL: u8 = mmp_sys::MMP_SENTINEL;

/// MMP Subsystem ids
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
pub enum Subsystem {
    ///  MMP interface configuration and control data/notifications
    IFace = mmp_sys::MMP_SUBSYS_IFACE,
    ///  S-Registers
    SReg = mmp_sys::MMP_SUBSYS_SREG,
    ///  Deck box-specific data/notifications
    Deckbox = mmp_sys::MMP_SUBSYS_DECKBOX,
    ///  Data packets and other general notifications
    Data = mmp_sys::MMP_SUBSYS_DATA,
    ///  Transponding and ranging data/notifications
    Transpond = mmp_sys::MMP_SUBSYS_TRANSPOND,
    ///  Time- and date-related data/notifications
    Time = mmp_sys::MMP_SUBSYS_TIME,
    /// Special subsystem for retrieving all values in a config section:
    /// set the field with the CFG section number to get or
    /// MMP_SUBSYS_CFG_ALL to get all sections. If used with a
    /// SET operation and MMP_SUBSYS_CFG_ALL is used for both
    /// the the subsys and field, the configuration database will be
    /// written to flash.
    ConfigAll = mmp_sys::MMP_SUBSYS_CFG_ALL,
    ///  Co-processor board
    ConfigCoProc = mmp_sys::CFG_SECT_COPROC,
    ///  On-board datalogger
    ConfigDatalog = mmp_sys::CFG_SECT_DATALOG,
    ///  Modem functionality
    ConfigModem = mmp_sys::CFG_SECT_MODEM,
    ///  Release functionality
    ConfigRelease = mmp_sys::CFG_SECT_RELEASE,
    ///  UART paramters
    ConfigSerial = mmp_sys::CFG_SECT_SERIAL,
    ///  System parameters
    ConfigSystem = mmp_sys::CFG_SECT_SYSTEM,
    ///  Test functionality
    ConfigTest = mmp_sys::CFG_SECT_TEST,
    ///  Version information
    ConfigVersion = mmp_sys::CFG_SECT_VERSION,
    ///  Transpond control
    ConfigXpnd = mmp_sys::CFG_SECT_XPND,
    ///  Global Pose Sensors
    ConfigNav = mmp_sys::CFG_SECT_NAV,
    ///  Data recorder configuration
    ConfigRecorder = mmp_sys::CFG_SECT_RECORDER,
    ///  Sonar Modem configuration
    ConfigSonar = mmp_sys::CFG_SECT_SONAR,
    ///  Sonar Modem User configuration
    ConfigUserSdk = mmp_sys::CFG_SECT_USER_SDK,
    ///  Transport layer routing/tagging information
    ConfigTransport = mmp_sys::CFG_SECT_TRANSPORT,
    ///  WHOI configuration
    ConfigWhoi = mmp_sys::CFG_SECT_WHOI,
    ///  Analog Input Option
    ConfigAnalogIn = mmp_sys::CFG_SECT_AIN,
    ///  Pressure gauge options (requires analog input)
    ConfigPressure = mmp_sys::CFG_SECT_PRESSURE,
    //CFG_SECT_USBL USBL control section
    ///  NUWC Ranging DPSK Integration section
    ConfigDpsk = mmp_sys::CFG_SECT_DPSK,
}

impl Subsystem {
    const ALL: &'static [Self] = &[
        Self::IFace,
        Self::SReg,
        Self::Deckbox,
        Self::Data,
        Self::Transpond,
        Self::Time,
        Self::ConfigAll,
        Self::ConfigCoProc,
        Self::ConfigDatalog,
        Self::ConfigModem,
        Self::ConfigRelease,
        Self::ConfigSerial,
        Self::ConfigSystem,
        Self::ConfigTest,
        Self::ConfigVersion,
        Self::ConfigXpnd,
        Self::ConfigNav,
        Self::ConfigRecorder,
        Self::ConfigSonar,
        Self::ConfigUserSdk,
        Self::ConfigTransport,
        Self::ConfigWhoi,
        Self::ConfigAnalogIn,
        Self::ConfigPressure,
        Self::ConfigDpsk,
    ];

    /// Iterate through all subsystem fields
    pub fn iter_fields() -> core::slice::Iter<'static, Self> {
        Self::ALL.iter()
    }
}

/// MMP Command Ids
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
pub enum Command {
    /// Null command section (unused)
    Null = mmp_sys::MMP_CMDSECT_NULL,
    /// Standard commands and operations supported on most modem-based platforms
    Std = mmp_sys::MMP_CMDSECT_STD,
    /// Ranging and releasing operations
    Rngrls = mmp_sys::MMP_CMDSECT_RNGRLS,
    /// Commands for datalogger
    Datalog = mmp_sys::MMP_CMDSECT_DATALOG,
}

pub use mmp_serialize::{from_bytes, to_bytes};
