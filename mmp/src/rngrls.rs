//! MMP Range and Release Commands

use core::time::Duration;
use mmp_serialize::{Deserialize, Serialize};
use num_enum::{IntoPrimitive, TryFromPrimitive};

const COMMAND: crate::Command = crate::Command::Rngrls;

#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Command {
    /// Issue interrogation or silent ping and enter
    /// transpond receive mode for the system
    /// default amount of time
    Transpond = mmp_sys::MMP_RNGRLS_CMD_TRANSPOND,
    /// Obtain the range to another modem
    Range = mmp_sys::MMP_RNGRLS_CMD_RANGE,
    /// Check the release mechanism status on a
    /// remote modem
    ReleaseStatus = mmp_sys::MMP_RNGRLS_CMD_RELEASE_STATUS,
    /// Issue a release command to a remote
    /// Benthos SMART release (SR-50, SR-100,
    /// etc.)
    SmartRelease = mmp_sys::MMP_RNGRLS_CMD_SMART_RELEASE,
    /// Issue a release command to a remote
    /// Benthos SMART modem (SM-75, OEM
    /// boardset w/ burn wire, etc.)
    Burnwire = mmp_sys::MMP_RNGRLS_CMD_BURNWIRE,
    /// Issue a release command to a remote
    /// Benthos FSK release (Model 865, etc.)
    FskRelease = mmp_sys::MMP_RNGRLS_CMD_FSK_RELEASE,
    /// Issue a command to an
    /// EdgeTech/ORE/EG&G release
    EdgetechRelease = mmp_sys::MMP_RNGRLS_CMD_EDGETECH_RELEASE,
    /// Issue a command to a University of Rhode
    /// Island release/device
    UriRelease = mmp_sys::MMP_RNGRLS_CMD_URI_RELEASE,
    /// Save per-frequency transpond sensitivity
    /// threshold adjustments to flash
    FlashXpndAdj = mmp_sys::MMP_RNGRLS_CMD_FLASH_XPNDADJ,
    /// Obtain bearing from a DAT
    Bearing = mmp_sys::MMP_RNGRLS_CMD_BEARING,
    /// Issue a command to a GeoPro (Germany)
    /// release unit
    GeoProRelease = mmp_sys::MMP_RNGRLS_CMD_GEOPRO_RELEASE,
    /// Obtain position information from a remote
    /// modem
    NavData = mmp_sys::MMP_RNGRLS_CMD_NAVDATA,
    /// probe channel for multipath
    ChannelProbe = mmp_sys::MMP_RNGRLS_CMD_CHANNEL_PROBE,
    /// Ranging specific to a Teledyne Benthos
    /// next-gen release
    RSeriesRange = mmp_sys::MMP_RNGRLS_CMD_RSERIES_RANGE,
    /// Activate an R Series release
    RSeriesActivate = mmp_sys::MMP_RNGRLS_CMD_RSERIES_ACTIVATE,
    /// Hibernate an R Series release
    RSeriesHibernate = mmp_sys::MMP_RNGRLS_CMD_RSERIES_HIBERNATE,
    /// Set the transpond reSponse mode in a
    /// remote R Series unit
    RSeriesXpndMode = mmp_sys::MMP_RNGRLS_CMD_RSERIES_XPND_MODE,
    /// Set the transpond reSponse mode in a
    /// remote standard unit
    StdXpndMode = mmp_sys::MMP_RNGRLS_CMD_STD_XPND_MODE,
    /// Enable R Series transponding
    RSeriesDBUnlock = mmp_sys::MMP_RNGRLS_CMD_RSERIES_DB_UNLOCK,
    /// Disable R Series transponding
    RSEriesDBLock = mmp_sys::MMP_RNGRLS_CMD_RSERIES_DB_LOCK,
    /// Issue a release command to a remote
    /// Benthos R Series release
    RSeriesRelease = mmp_sys::MMP_RNGRLS_CMD_RSERIES_RELEASE,
    /// Issue pulse on binary out 1 to a responder
    ResponderRange = mmp_sys::MMP_RNGRLS_CMD_RESPONDER_RANGE,
    /// Issue interrogation or silent ping and enter
    /// transpond receive mode for a specified
    /// amount of time
    TranspondWithTimeout = mmp_sys::MMP_RNGRLS_CMD_TRANSPOND_W_TIMEOUT,
    /// Issue a range request with a timeout
    /// specified (not system @AcRspTmOut
    /// default)
    RangeWithTimeout = mmp_sys::MMP_RNGRLS_CMD_RANGE_W_TIMEOUT,
    /// Enter or exit spectrum analysis mode, with
    /// update rate in 0.5 second increments (or 0
    /// to disable)
    SpectrumMode = mmp_sys::MMP_RNGRLS_CMD_SPECTRUM_MODE,
    /// Request the high precision abolute and relative bearing
    AbsRel = mmp_sys::MMP_RNGRLS_CMD_ABSREL,
    /// Record a 1 second basebanded in-band
    /// acoustic sample
    AcousticSample = 0x1D, // in datasheet but missing from c header
    /// Request the high precision compass and
    /// depth
    AbsCompassDepth = 0x1E, // in datasheet but missing from c header
}

#[derive(Debug, PartialEq)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct TranspondWithTimeout {
    /// Frequency of interrogation ping in Hz
    pub freq: u32,
    /// True for silent interrogation ping
    pub silent: bool,
    /// True to enter transpond receive mode with no ping
    pub receive_only: bool,
    /// The listen period timeout (resolution of 0.1 seconds)
    pub timeout: Duration,
}

impl<'a> Serialize<'a> for TranspondWithTimeout {
    fn serialize(
        &self,
        serializer: mmp_serialize::ser::Serializer<'a>,
    ) -> Result<mmp_serialize::ser::Serializer<'a>, mmp_serialize::Error> {
        serializer
            .serialize_u8(COMMAND.into())
            .and_then(|s| s.serialize_u8(Command::TranspondWithTimeout.into()))
            .and_then(|s| s.start_length())
            .and_then(|s| s.serialize_u32(self.freq))
            .and_then(|s| s.serialize_u16(self.silent as u16))
            .and_then(|s| s.serialize_u16(self.receive_only as u16))
            .and_then(|s| s.serialize_u16((self.timeout.as_millis() / 100) as u16))
            .and_then(|s| s.finish_length())
    }
}

impl<'de> Deserialize<'de> for TranspondWithTimeout {
    fn deserialize(
        deserializer: mmp_serialize::de::Deserializer<'de>,
    ) -> Result<(mmp_serialize::de::Deserializer<'de>, Self), mmp_serialize::Error> {
        let (deserializer, system) = deserializer.deserialize_u8()?;
        if system != COMMAND.into() {
            return Err(mmp_serialize::Error::SubsystemMismatch);
        }

        let (deserializer, field) = deserializer.deserialize_u8()?;
        if field != Command::TranspondWithTimeout.into() {
            return Err(mmp_serialize::Error::FieldMismatch);
        }

        let (deserializer, _len) = deserializer.deserialize_u16()?;
        let (deserializer, freq) = deserializer.deserialize_u32()?;
        let (deserializer, silent) = deserializer.deserialize_u16().map(|(d, v)| (d, v != 0))?;
        let (deserializer, receive_only) =
            deserializer.deserialize_u16().map(|(d, v)| (d, v != 0))?;
        let (deserializer, timeout) = deserializer
            .deserialize_u16()
            .map(|(d, v)| (d, Duration::from_millis(v as u64 * 100)))?;

        Ok((
            deserializer,
            Self {
                freq,
                silent,
                receive_only,
                timeout,
            },
        ))
    }
}

mod de {
    #![allow(unused_imports)]

    use super::*;
    use mmp_serialize::from_bytes;

    #[test]
    fn deserialize_transpond_with_timeout() {
        let freq = 12000;
        let silent = true;
        let receive_only = false;
        let timeout = Duration::from_secs(1);

        let expected = TranspondWithTimeout {
            freq,
            silent,
            receive_only,
            timeout,
        };

        let mut data = [0u8; 14];
        data[0] = COMMAND.into();
        data[1] = Command::TranspondWithTimeout.into();
        data[2..4].copy_from_slice(&10u16.to_be_bytes());
        data[4..8].copy_from_slice(&(freq as u32).to_be_bytes());
        data[8..10].copy_from_slice(&(silent as u16).to_be_bytes());
        data[10..12].copy_from_slice(&(receive_only as u16).to_be_bytes());
        data[12..14].copy_from_slice(&((timeout.as_millis() / 100) as u16).to_be_bytes());

        let (tail, result) = from_bytes(&data).unwrap();
        assert_eq!(0, tail.len());
        assert_eq!(expected, result);
    }
}

mod ser {
    #![allow(unused_imports)]
    use super::*;
    use mmp_serialize::to_bytes;

    #[test]
    fn serialize_transpond_with_timeout() {
        let freq = 9000;
        let silent = false;
        let receive_only = true;
        let timeout = Duration::from_secs(10);

        let value = TranspondWithTimeout {
            freq,
            silent,
            receive_only,
            timeout,
        };

        let mut buf = [0u8; 16];
        let (result, _tail) = to_bytes(&value, &mut buf).unwrap();

        let mut expected = [0u8; 16];
        expected[0] = COMMAND.into();
        expected[1] = Command::TranspondWithTimeout.into();
        expected[2..4].copy_from_slice(&10u16.to_be_bytes());
        expected[4..8].copy_from_slice(&freq.to_be_bytes());
        expected[8..10].copy_from_slice(&(silent as u16).to_be_bytes());
        expected[10..12].copy_from_slice(&(receive_only as u16).to_be_bytes());
        expected[12..14].copy_from_slice(&((timeout.as_millis() / 100) as u16).to_be_bytes());
        assert_eq!(&expected[..14], result);
    }
}
