/// Pressure Gauge options (requires analog input)
use num_enum::{IntoPrimitive, TryFromPrimitive};
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    /// Analog input channel where the
    /// pressure transducer is connected,
    /// or 0 if none
    AinChannel = mmp_sys::CFG_PRESSURE_AINCHNL,

    /// Conversion factor for pressure,
    /// number of meters per psi
    MetersPerPsi = mmp_sys::CFG_PRESSURE_METERSPERPSI,
    /// Atmospheric pressure in PSI to be
    /// subtracted from pressure reading
    /// when converting to depth below
    /// sea level
    AtmOffset = mmp_sys::CFG_PRESSURE_ATMOFFSET,

    /// The pressure in pounds per square
    /// inch (psi) as set by user or updated
    /// from pressure gauge
    Psi = mmp_sys::CFG_PRESSURE_PSI,
}
