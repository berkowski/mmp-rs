/// System Paremeters
use crate::config::Enum;
use mmp_serialize::{
    de::{Deserialize, Deserializer},
    ser::{Serialize, Serializer},
    Error,
};

use num_enum::{IntoPrimitive, TryFromPrimitive};

#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ///  Enable auxiliary acoustic input
    AuxInput = mmp_sys::CFG_SYSTEM_AUXINP,
    ///  Received data output in standard ASCII or as a hex byte dump
    AsciiBin = mmp_sys::CFG_SYSTEM_ASCIIBIN,
    Bandwidth = mmp_sys::CFG_SYSTEM_BANDWIDTH,
    /// Center carrier frequency (sets
    /// acoustic band)
    CarrierFreq = mmp_sys::CFG_SYSTEM_CARRFREQ,
    ///  Compact modem reset:
    CMReset = mmp_sys::CFG_SYSTEM_COMPMDMRST,
    /// Compact modem wakeup
    /// hibernate time (formerly last digit
    /// of S-10 when >300)
    CMWakeup = mmp_sys::CFG_SYSTEM_CMWAKEHIB,
    /// Compact modem wakeup active
    /// listen time (formerly middle digit of
    /// S-10 when >400)
    CMWakeListen = mmp_sys::CFG_SYSTEM_CMWAKELISTEN,
    /// Half bandwidth modulation: 1 =
    /// Normal, 2 = half bandwidth
    HalfBwModlation = mmp_sys::CFG_SYSTEM_HALFBW,
    ///  Frequency Hop receive threshold
    FreqHopTresh = mmp_sys::CFG_SYSTEM_FHTHRESH,
    /// Configuration of release type
    /// (factory-set)
    RlsType = mmp_sys::CFG_SYSTEM_RLSTYPE,
    /// Mode for 1 PPS clock signal
    /// syncing
    SyncPps = mmp_sys::CFG_SYSTEM_SYNCPPS,
    ///  console status verbosity
    Verbose = mmp_sys::CFG_SYSTEM_VERBOSE,
    WakeThresh = mmp_sys::CFG_SYSTEM_WAKETHRESH,
    /// Control behavior of auxiliary
    /// acoustic output: either default
    /// behavior (speaker/phones for
    /// UDB, copies of samples for
    /// others), or force copies of
    /// outbound samples to aux port.
    AuxOut = mmp_sys::CFG_SYSTEM_AUXOUT,
    /// Use an analog linear pot on EXT_SENSE1_ADC,
    /// to control
    /// speaker and headphone volume
    VolCont = mmp_sys::CFG_SYSTEM_VOLCONT,
    /// Enables fast wake from hibernate
    /// ability on compact modem
    /// (receiving side) and UDB (sending
    /// side).
    CMFastWake = mmp_sys::CFG_SYSTEM_CMFASTWAKE,
    /// Low-power idle timer
    IdleTimer = mmp_sys::CFG_SYSTEM_IDLETIMER,
    /// copy RTC PPS out on BIN_OUT_0
    SyncOut = mmp_sys::CFG_SYSTEM_SYNCOUT,
    /// BIN_IN_0 pullup
    PullUp0 = mmp_sys::CFG_SYSTEM_PULLUP0,
    /// BIN_IN_1 pullup
    PullUp1 = mmp_sys::CFG_SYSTEM_PULLUP1,
    /// The operating voltage threshold
    /// below which transmissions will be
    /// automatically reduced in power to
    /// prevent brown-out resets
    MinOpVolt = mmp_sys::CFG_SYSTEM_MINOPVOLT,
    /// The type of battery being used to
    /// power the modem (Standard
    /// alkaline, Smart Li+, Lithium
    /// primary, etc.)
    BatteryType = mmp_sys::CFG_SYSTEM_BATTERYTYPE,
    /// The capacity of the installed
    /// battery pack in watt-hours. Only
    /// for certain release products with
    /// battery monitoring capability.
    BatteryCapacity = mmp_sys::CFG_SYSTEM_BATTERYCAPACITY,
    /// Manufacture date of the battery
    /// specifying month and year. Only
    /// for certain release products with
    /// battery monitoring capability.
    BatteryMfgDate = mmp_sys::CFG_SYSTEM_BATTERYMFGDATE,
    /// The axis of the built-in
    /// accelerometer to be used as the
    /// tilt/pitch reference
    TiltAxis = mmp_sys::CFG_SYSTEM_TILTAXIS,
    /// The Power On Timer used for
    /// Releases
    PwrOnTimer = mmp_sys::CFG_SYSTEM_PWRONTIMER,
    /// The Awake Timer used for
    /// Releases
    AwakeTimer = mmp_sys::CFG_SYSTEM_AWAKETIMER,
    /// The Acoustic Release Hibernate
    /// Sleep period
    AcWakeHibernate = mmp_sys::CFG_SYSTEM_ARWAKEHIB,
    ///  hydrophone sensitivity in dB (uPa) for reference only
    RxSens = mmp_sys::CFG_SYSTEM_RXSENS,
    /// keep TR board 12V and 3.3V on
    /// when in active receive
    AwakePower = mmp_sys::CFG_SYSTEM_AWAKEPOWER,
}

/// Type of release
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum ReleaseType {
    None = 0,
    SmartRelease = 1,
    SmartModem = 2,
    OEMBurnWire = 3,
    SmartOEM = 8,
}

impl<'a> Serialize<'a> for ReleaseType {
    fn serialize(&self, serializer: Serializer<'a>) -> Result<Serializer<'a>, Error> {
        Enum::serialize(&Enum(*self as i32), serializer)
    }
}

impl<'de> Deserialize<'de> for ReleaseType {
    fn deserialize(
        deserializer: Deserializer<'de>,
    ) -> core::result::Result<(Deserializer<'de>, Self), Error> {
        let (deserializer, enum_) = Enum::deserialize(deserializer)?;
        let enum_ = Self::try_from_primitive(enum_.0 as u8)
            //.map_err(|_| serde::de::Error::custom("unknown release type value"))
            .map_err(|_| Error::Deserialize)?;

        Ok((deserializer, enum_))
    }
}
