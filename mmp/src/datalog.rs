/// On-board datalogger
use num_enum::{IntoPrimitive, TryFromPrimitive};
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    /// Log acoustically received data to
    /// datalogger
    AcData = mmp_sys::CFG_DATALOG_ACDATA,
    ///  Log acoustic statistics to datalogger
    AcStats = mmp_sys::CFG_DATALOG_ACSTATS,
    /// Configure datalogger as flat (stop when
    /// full) or circular (oldest records discarded
    /// for newer)
    RingBuf = mmp_sys::CFG_DATALOG_RINGBUF,
    /// The record-partitioning mode for the data
    /// logger: time, character, or size based.
    LogMode = mmp_sys::CFG_DATALOG_LOGMODE,
    /// The ASCII value of a sentinel character
    /// that will trigger closure of a discrete record
    /// in the data logger
    Sentinel = mmp_sys::CFG_DATALOG_SENTINEL,
    /// The number of characters that must be
    /// reached in order to trigger closure of a
    /// discrete record in the data logger
    CharCount = mmp_sys::CFG_DATALOG_CHRCOUNT,
    /// Indicates which storage medium the data
    /// logger points to (internal or external
    /// SDHC)
    LogStore = mmp_sys::CFG_DATALOG_LOGSTORE,
}
