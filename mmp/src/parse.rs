#![allow(unused)]

use crate::types;

use core::convert::TryFrom;
/// Check if byte is the message sentinel character
fn is_sentinel(b: u8) -> bool {
    b == types::MMP_SENTINEL
}

/// Parse the transaction ID (xid)
fn xid(i: &[u8]) -> nom::IResult<&[u8], u8> {
    nom::number::streaming::u8(i)
}

/// Parse MMP operation type
fn operation_type(i: &[u8]) -> nom::IResult<&[u8], types::MmpType> {
    nom::combinator::map_res(nom::number::streaming::u8, types::MmpType::try_from)(i)
}

/// Number of parameter groups
fn num_groups(i: &[u8]) -> nom::IResult<&[u8], u8> {
    nom::number::streaming::u8(i)
}

/// Parse the subsystem type
fn subsystem(i: &[u8]) -> nom::IResult<&[u8], types::SubSystem> {
    nom::combinator::map_res(nom::number::streaming::u8, |b| {
        types::SubSystem::try_from(b).map_err(types::Error::from)
    })(i)
}

/// Parse the subsystem/subfield two-byte combination
fn subsystem_and_field(i: &[u8]) -> nom::IResult<&[u8], types::Parameter> {
    use types::{Parameter, SubSystem};

    let (i, subsystem) = subsystem(i)?;
    nom::combinator::map_res(
        nom::number::streaming::u8,
        move |field| -> Result<_, types::Error> {
            match subsystem {
                SubSystem::ConfigAll => Ok(Parameter::ConfigAll),
                SubSystem::IFace => types::IFaceField::try_from(field)
                    .map(Parameter::IFace)
                    .map_err(types::Error::from),
                SubSystem::SReg => types::SRegField::try_from(field)
                    .map(Parameter::SReg)
                    .map_err(types::Error::from),
                SubSystem::Deckbox => types::DeckboxField::try_from(field)
                    .map(Parameter::Deckbox)
                    .map_err(types::Error::from),
                SubSystem::Data => types::DataField::try_from(field)
                    .map(Parameter::Data)
                    .map_err(types::Error::from),
                SubSystem::Transpond => types::TranspondField::try_from(field)
                    .map(Parameter::Transpond)
                    .map_err(types::Error::from),
                SubSystem::Time => types::TimeField::try_from(field)
                    .map(Parameter::Time)
                    .map_err(types::Error::from),
                SubSystem::ConfigCoProc => types::ConfigCoProcField::try_from(field)
                    .map(Parameter::ConfigCoProc)
                    .map_err(types::Error::from),
                SubSystem::ConfigDatalog => types::ConfigDatalogField::try_from(field)
                    .map(Parameter::ConfigDatalog)
                    .map_err(types::Error::from),
                SubSystem::ConfigModem => types::ConfigModemField::try_from(field)
                    .map(Parameter::ConfigModem)
                    .map_err(types::Error::from),
                SubSystem::ConfigRelease => types::ConfigReleaseField::try_from(field)
                    .map(Parameter::ConfigRelease)
                    .map_err(types::Error::from),
                SubSystem::ConfigSerial => types::ConfigSerialField::try_from(field)
                    .map(Parameter::ConfigSerial)
                    .map_err(types::Error::from),
                SubSystem::ConfigSystem => types::ConfigSystemField::try_from(field)
                    .map(Parameter::ConfigSystem)
                    .map_err(types::Error::from),
                SubSystem::ConfigTest => types::ConfigTestField::try_from(field)
                    .map(Parameter::ConfigTest)
                    .map_err(types::Error::from),
                SubSystem::ConfigVersion => types::ConfigVersionField::try_from(field)
                    .map(Parameter::ConfigVersion)
                    .map_err(types::Error::from),
                SubSystem::ConfigXpnd => types::ConfigXpndField::try_from(field)
                    .map(Parameter::ConfigXpnd)
                    .map_err(types::Error::from),
                SubSystem::ConfigNav => types::ConfigNavField::try_from(field)
                    .map(Parameter::ConfigNav)
                    .map_err(types::Error::from),
                SubSystem::ConfigRecorder => types::ConfigRecorderField::try_from(field)
                    .map(Parameter::ConfigRecorder)
                    .map_err(types::Error::from),
                SubSystem::ConfigSonar => types::ConfigSonarField::try_from(field)
                    .map(Parameter::ConfigSonar)
                    .map_err(types::Error::from),
                SubSystem::ConfigUserSdk => types::ConfigUserSdkField::try_from(field)
                    .map(Parameter::ConfigUserSdk)
                    .map_err(types::Error::from),
                SubSystem::ConfigTransport => types::ConfigTransportField::try_from(field)
                    .map(Parameter::ConfigTransport)
                    .map_err(types::Error::from),
                SubSystem::ConfigWhoi => types::ConfigWhoiField::try_from(field)
                    .map(Parameter::ConfigWhoi)
                    .map_err(types::Error::from),
                SubSystem::ConfigAnalogIn => types::ConfigAnalogInField::try_from(field)
                    .map(Parameter::ConfigAnalogIn)
                    .map_err(types::Error::from),
                SubSystem::ConfigPressure => types::ConfigPressureField::try_from(field)
                    .map(Parameter::ConfigPressure)
                    .map_err(types::Error::from),
                SubSystem::ConfigDpsk => types::ConfigDpskField::try_from(field)
                    .map(Parameter::ConfigDpsk)
                    .map_err(types::Error::from),
            }
        },
    )(i)
}

/// Data length is given as a two-byte big-endian value
fn data_length(i: &[u8]) -> nom::IResult<&[u8], usize> {
    nom::combinator::map(nom::number::streaming::be_u16, |u| u as usize)(i)
}

/// A ParameterGroup with parsed Subsystem/Field combination and unparsed data payload
#[derive(Debug, PartialEq)]
struct ParameterGroup<'a> {
    pub subsystem: types::Parameter,
    pub data: &'a [u8],
}

/// Parse a parameter group
fn parameter_group(i: &[u8]) -> nom::IResult<&[u8], ParameterGroup> {
    let (i, subsystem) = subsystem_and_field(i)?;
    nom::combinator::map(nom::multi::length_data(data_length), move |data| {
        ParameterGroup { subsystem, data }
    })(i)
}

/// A ParameterGroup with parsed Subsystem/Field combination and unparsed data payload
#[derive(Debug, PartialEq)]
struct Command<'a> {
    pub command: types::Command,
    pub data: &'a [u8],
}

/// Parse a parameter group
fn command(i: &[u8]) -> nom::IResult<&[u8], Command> {
    let (i, command) = section_and_command(i)?;
    nom::combinator::map(nom::multi::length_data(data_length), move |data| Command {
        command,
        data,
    })(i)
}

/// Parse a command type (section)
fn section(i: &[u8]) -> nom::IResult<&[u8], types::CommandType> {
    nom::combinator::map_res(nom::number::streaming::u8, |b| {
        types::CommandType::try_from(b)
    })(i)
}

/// Parse the section and command
fn section_and_command(i: &[u8]) -> nom::IResult<&[u8], types::Command> {
    use types::{Command, CommandType};

    let (i, section) = section(i)?;
    nom::combinator::map_res(
        nom::number::streaming::u8,
        move |command| -> Result<_, types::Error> {
            match section {
                CommandType::Std => types::StdCommand::try_from(command)
                    .map(Command::Std)
                    .map_err(types::Error::from),
                CommandType::RngRls => types::RngRlsCommand::try_from(command)
                    .map(Command::RngRls)
                    .map_err(types::Error::from),
                CommandType::Datalog => types::DatalogCommand::try_from(command)
                    .map(Command::Datalog)
                    .map_err(types::Error::from),
            }
        },
    )(i)
}
#[cfg(test)]
mod test {
    pub use super::*;
    pub use crate::types::*;

    #[test]
    fn test_is_sentinel() {
        assert!(is_sentinel(types::MMP_SENTINEL));
        assert!(!is_sentinel(4));
    }

    /// Parameter group parsing examples from mmpd manual
    mod parse_groups {
        use super::*;

        #[test]
        fn incomplete_payload() {
            assert_eq!(
                parameter_group(&b"\x02\x00\x00\x02\x80"[..]),
                Err(nom::Err::Incomplete(nom::Needed::Size(
                    core::num::NonZeroUsize::new(1).unwrap()
                )))
            )
        }

        #[test]
        fn bad_subsystem_byte() {
            assert_eq!(
                parameter_group(&b"\xFF\x00\x00\x02\x80\x00"[..]),
                Err(nom::Err::Error(nom::error::Error {
                    input: &b"\xFF\x00\x00\x02\x80\x00"[..],
                    code: nom::error::ErrorKind::MapRes
                }))
            )
        }

        #[test]
        fn bad_subsystem_field_byte() {
            assert_eq!(
                parameter_group(&b"\x02\xEE\x00\x02\x80\x00"[..]),
                Err(nom::Err::Error(nom::error::Error {
                    input: &b"\xEE\x00\x02\x80\x00"[..],
                    code: nom::error::ErrorKind::MapRes
                }))
            )
        }

        #[test]
        fn fig4_pg11() {
            assert_eq!(
                parameter_group(&b"\x02\x00\x00\x02\x80\x00"[..]),
                Ok((
                    &b""[..],
                    ParameterGroup {
                        subsystem: Parameter::Deckbox(DeckboxField::SpeakerVolume),
                        data: &b"\x80\x00"[..]
                    }
                ))
            )
        }

        #[test]
        fn fig12_pg32() {
            assert_eq!(
                parameter_group(&b"\x00\x02\x00\x04\x00\x01\x00\x00"[..]),
                Ok((
                    &b""[..],
                    ParameterGroup {
                        subsystem: Parameter::IFace(IFaceField::Version),
                        data: &b"\x00\x01\x00\x00"[..]
                    }
                ))
            )
        }

        #[test]
        fn fig14_pg33() {
            assert_eq!(
                parameter_group(&b"\x02\x01\x00\x02\x80\x00"[..]),
                Ok((
                    &b""[..],
                    ParameterGroup {
                        subsystem: Parameter::Deckbox(DeckboxField::PhonesVolume),
                        data: &b"\x80\x00"[..]
                    }
                ))
            )
        }

        #[test]
        fn fig16_pg34() {
            assert_eq!(
                parameter_group(&b"\x01\x06\x00\x02\x00\x06"[..]),
                Ok((
                    &b""[..],
                    ParameterGroup {
                        subsystem: Parameter::SReg(SRegField::TxPower),
                        data: &b"\x00\x06"[..]
                    }
                ))
            )
        }

        #[test]
        fn fig17_pg35() {
            assert_eq!(
                parameter_group(&b"\x02\x00\x00\x02\x3F\x01\x05\x09\x00\x02\xFF\xE7"[..]),
                Ok((
                    &b"\x05\x09\x00\x02\xFF\xE7"[..],
                    ParameterGroup {
                        subsystem: Parameter::Deckbox(DeckboxField::SpeakerVolume),
                        data: &b"\x3F\x01"[..]
                    }
                ))
            )
        }

        #[test]
        fn transpond_w_timeout() {
            assert_eq!(
                command(&b"\x02\x16\x00\x0A\x00\x00\x23\x28\x00\x00\x00\x00\x00\x01"[..]),
                Ok((
                    &b""[..],
                    Command {
                        command: types::Command::RngRls(RngRlsCommand::TranspondWTimeout),
                        data: &b"\x00\x00\x23\x28\x00\x00\x00\x00\x00\x01"[..]
                    }
                ))
            )
        }
    }
}
