//! Benthos Modem Message Protocol handling

use crate::{log, MessageKind, Subsystem, MMP_SENTINEL};
use core::convert::{TryFrom, TryInto};

#[derive(Debug, PartialEq)]
pub struct BufferFull {}

#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
pub enum Group<'a> {
    AnalogIn {
        field: crate::analog::Field,
        data: &'a [u8],
    },
    ConfigAll {
        data: &'a [u8],
    },
    CoProc {
        field: crate::coproc::Field,
        data: &'a [u8],
    },
    Data {
        field: crate::data::Field,
        data: &'a [u8],
    },
    Datalog {
        field: crate::datalog::Field,
        data: &'a [u8],
    },
    Deckbox {
        field: crate::deckbox::Field,
        data: &'a [u8],
    },
    Dpsk {
        field: crate::dpsk::Field,
        data: &'a [u8],
    },
    IFace {
        field: crate::iface::Field,
        data: &'a [u8],
    },
    Modem {
        field: crate::modem::Field,
        data: &'a [u8],
    },
    Nav {
        field: crate::nav::Field,
        data: &'a [u8],
    },
    Pressure {
        field: crate::pressure::Field,
        data: &'a [u8],
    },
    Recorder {
        field: crate::recorder::Field,
        data: &'a [u8],
    },
    Release {
        field: crate::release::Field,
        data: &'a [u8],
    },
    Sdk {
        field: crate::sdk::Field,
        data: &'a [u8],
    },
    Serial {
        field: crate::serial::Field,
        data: &'a [u8],
    },
    Sonar {
        field: crate::sonar::Field,
        data: &'a [u8],
    },
    SReg {
        field: crate::sreg::Field,
        data: &'a [u8],
    },
    System {
        field: crate::system::Field,
        data: &'a [u8],
    },
    Test {
        field: crate::test::Field,
        data: &'a [u8],
    },
    Time {
        field: crate::time::Field,
        data: &'a [u8],
    },
    Transpond {
        field: crate::transpond::Field,
        data: &'a [u8],
    },
    Transport {
        field: crate::transport::Field,
        data: &'a [u8],
    },
    Version {
        field: crate::version::Field,
        data: &'a [u8],
    },
    Whoi {
        field: crate::whoi::Field,
        data: &'a [u8],
    },
    Xpnd {
        field: crate::xpnd::Field,
        data: &'a [u8],
    },
    Other {
        system: u8,
        field: u8,
        data: &'a [u8],
    },
}

#[derive(Clone, PartialEq, Debug)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
pub enum Error {
    InvalidHeader,
    /// Size mismatch
    Size {
        expected: usize,
        actual: usize,
    },
}

impl<'a> TryFrom<&'a [u8]> for Group<'a> {
    type Error = Error;

    fn try_from(data: &'a [u8]) -> Result<Self, Self::Error> {
        if data.len() < 4 {
            return Err(Self::Error::InvalidHeader);
        }

        //let (header, data) = b.split_at(4);

        let system_u8 = data[0];
        let field_u8 = data[1];
        let expected = u16::from_be_bytes(data[2..4].try_into().unwrap()) as usize + 4;
        let actual = data.len();
        if expected != actual {
            return Err(Self::Error::Size { expected, actual });
        }

        match Subsystem::try_from(system_u8) {
            Ok(system) => match system {
                Subsystem::IFace => match crate::iface::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::IFace { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::SReg => match crate::sreg::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::SReg { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::Deckbox => match crate::deckbox::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Deckbox { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::Data => match crate::data::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Data { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::Transpond => match crate::transpond::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Transpond { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::Time => match crate::time::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Time { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigCoProc => match crate::coproc::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::CoProc { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigAnalogIn => match crate::analog::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::AnalogIn { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigDatalog => match crate::datalog::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Datalog { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigModem => match crate::modem::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Modem { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigNav => match crate::nav::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Nav { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigPressure => match crate::pressure::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Pressure { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigRecorder => match crate::recorder::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Recorder { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigRelease => match crate::release::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Release { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigSerial => match crate::serial::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Serial { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigUserSdk => match crate::sdk::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Sdk { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigDpsk => match crate::dpsk::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Dpsk { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigSonar => match crate::sonar::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Sonar { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigSystem => match crate::system::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::System { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigTest => match crate::test::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Test { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigTransport => match crate::transport::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Transport { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigVersion => match crate::version::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Version { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigWhoi => match crate::whoi::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Whoi { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigXpnd => match crate::xpnd::Field::try_from(field_u8) {
                    Ok(field) => Ok(Group::Xpnd { field, data }),
                    Err(_) => Ok(Group::Other {
                        system: system_u8,
                        field: field_u8,
                        data,
                    }),
                },
                Subsystem::ConfigAll => Ok(Group::ConfigAll { data }),
            },
            _ => Ok(Group::Other {
                system: system_u8,
                field: field_u8,
                data,
            }),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
pub struct Notification<'a> {
    pub xid: u8,
    pub group: Group<'a>,
}

enum State {
    Idle,
    ReadHeader,
    ReadGroup {
        xid: u8,
        group_size: Option<usize>,
        groups_left: usize,
    },
    ReadNextGroup {
        xid: u8,
        groups_left: usize,
    },
}

pub struct MmpReader<const N: usize> {
    buffer: [u8; N],
    index: usize,
    state: State,
}

impl<const N: usize> MmpReader<N> {
    const READ_HEADER_SIZE: usize = 3;
    const READ_GROUP_HEADER_SIZE: usize = 4;
    const CAPACITY: usize = N;

    /// Create a MmpReader of size N
    #[inline(always)]
    pub const fn new() -> Self {
        Self {
            buffer: [0u8; N],
            index: 0,
            state: State::Idle,
        }
    }

    fn push(&mut self, byte: u8) -> Result<(), BufferFull> {
        if self.index < Self::CAPACITY {
            self.buffer[self.index] = byte;
            self.index += 1;
            log::trace! {"push byte {:#X}, buffer length: {}", byte, self.index}
            Ok(())
        } else {
            Err(BufferFull {})
        }
    }

    // fn push_bytes(&mut self, bytes: &[u8]) -> Result<(), BufferFull> {
    //     let required = bytes.len();
    //     if bytes.len() > Self::CAPACITY {
    //         Err(BufferFull {})
    //     } else if required <= Self::CAPACITY - bytes.len() {
    //         self.buffer[self.index..self.index + required].copy_from_slice(bytes);
    //         self.index += required;
    //         Ok(())
    //     } else {
    //         Err(BufferFull {})
    //     }
    // }

    fn clear(&mut self) {
        log::trace! {"clear reader buffer"}
        self.index = 0;
    }

    /// Consume a byte
    ///
    /// parameter_group() should be run after this method on the returned reader object
    /// to check if a full parameter group has been successfully read.
    pub fn consume_byte(&mut self, byte: u8) -> Result<Option<Notification>, BufferFull> {
        let (next_state, maybe_notification) = match self.state {
            //
            // Idle state transitions: move to ReadHeader if byte is the MMP SENTINEL
            //
            State::Idle => {
                if byte == crate::MMP_SENTINEL {
                    self.clear();
                    (State::ReadHeader, None)
                } else {
                    (State::Idle, None)
                }
            }

            //
            // Read 3 byte message header.  If byte #2 does not match OpType Notify then
            // go back to Idle state
            //
            State::ReadHeader => {
                self.push(byte)?;

                if self.index < Self::READ_HEADER_SIZE {
                    (State::ReadHeader, None)
                } else if let Ok(MessageKind::Notify) = MessageKind::try_from(self.buffer[1]) {
                    let xid = self.buffer[0];
                    let groups_left = self.buffer[2] as usize;
                    self.clear();
                    (
                        State::ReadGroup {
                            xid,
                            groups_left,
                            group_size: None,
                        },
                        None,
                    )
                } else {
                    log::info! {"skipping non-notify message kind, header: {:?}", &self.buffer[..self.index]};
                    (State::Idle, None)
                }
            }
            State::ReadGroup {
                xid,
                mut groups_left,
                mut group_size,
            } => {
                self.push(byte)?;

                match group_size {
                    None => {
                        if self.index == Self::READ_GROUP_HEADER_SIZE {
                            group_size
                                .replace(u16::from_be_bytes(self.buffer[2..4].try_into().unwrap())
                                    as usize);
                        }
                        (
                            State::ReadGroup {
                                xid,
                                groups_left,
                                group_size,
                            },
                            None,
                        )
                    }
                    Some(size) => {
                        if self.index < size + Self::READ_GROUP_HEADER_SIZE {
                            (
                                State::ReadGroup {
                                    xid,
                                    groups_left,
                                    group_size,
                                },
                                None,
                            )
                        } else {
                            let notification = Group::try_from(&self.buffer[..self.index])
                                .map(|group| Notification { xid, group })
                                .ok();

                            groups_left -= 1;

                            (State::ReadNextGroup { xid, groups_left }, notification)
                        }
                    }
                }
            }
            State::ReadNextGroup { xid, groups_left } => {
                self.clear();
                if groups_left > 0 {
                    self.push(byte)?;
                    (
                        State::ReadGroup {
                            xid,
                            groups_left,
                            group_size: None,
                        },
                        None,
                    )
                } else if byte == MMP_SENTINEL {
                    (State::ReadHeader, None)
                } else {
                    (State::Idle, None)
                }
            }
        };
        self.state = next_state;
        Ok(maybe_notification)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn skip_welcome_message() {
        let message = &b"\nTeledyne Benthos Smart Release\nLF Frequency Band\nStandard version 8.14.0\nFeb  7 2022 21:32:02 \n\x24\x15\x00\x14\x97\x40\x04\x6E\x02\x02\x00\x00\x02\x3F\x01"[..];
        let mut reader = MmpReader::<128>::new();
        for byte in &message[..message.len() - 1] {
            assert!(reader.consume_byte(*byte).unwrap().is_none())
        }

        let expected = Ok(Some(Notification {
            xid: 0x04,
            group: Group::Deckbox {
                field: crate::deckbox::Field::SpeakerVolume,
                data: &b"\x02\x00\x00\x02\x3F\x01"[..],
            },
        }));

        let result = reader.consume_byte(*message.last().unwrap());
        assert_eq!(expected, result);
    }
    #[test]
    fn test_idle_search() {
        let message = &b"\x24\x15\x00\x14\x97\x40\x04\x6E\x02\x02\x00\x00\x02\x3F\x01"[..];
        let mut reader = MmpReader::<64>::new();
        for byte in &message[..message.len() - 1] {
            assert!(reader.consume_byte(*byte).unwrap().is_none())
        }

        let expected = Ok(Some(Notification {
            xid: 0x04,
            group: Group::Deckbox {
                field: crate::deckbox::Field::SpeakerVolume,
                data: &b"\x02\x00\x00\x02\x3F\x01"[..],
            },
        }));

        let result = reader.consume_byte(*message.last().unwrap());
        assert_eq!(expected, result);
    }

    #[test]
    fn test_non_notify_opcode() {
        let message = &b"\x40\x04\x6F\x02\x02\x00\x00\x02\x3F\x01"[..];
        let mut reader: MmpReader<64> = MmpReader::new();
        for byte in message {
            assert!(reader.consume_byte(*byte).unwrap().is_none());
        }
    }

    #[test]
    fn test_multiple() {
        let header_and_first_group = &b"\x40\x04\x6E\x02\x02\x00\x00\x02\x3F\x01"[..];
        let second_group = &b"\x05\x09\x00\x02\xFF\xE7"[..];
        let mut reader: MmpReader<64> = MmpReader::new();
        for byte in &header_and_first_group[..header_and_first_group.len() - 1] {
            assert!(reader.consume_byte(*byte).unwrap().is_none());
        }

        let expected = Ok(Some(Notification {
            xid: 0x04,
            group: Group::Deckbox {
                field: crate::deckbox::Field::SpeakerVolume,
                data: &b"\x02\x00\x00\x02\x3F\x01"[..],
            },
        }));

        let result = reader.consume_byte(*header_and_first_group.last().unwrap());

        assert_eq!(expected, result);

        for byte in &second_group[..second_group.len() - 1] {
            assert!(reader.consume_byte(*byte).unwrap().is_none());
        }

        let expected = Ok(Some(Notification {
            xid: 0x04,
            group: Group::Transpond {
                field: crate::transpond::Field::RxAdj7,
                data: &b"\x05\x09\x00\x02\xFF\xE7"[..],
            },
        }));

        let result = reader.consume_byte(*second_group.last().unwrap());
        assert_eq!(expected, result);
    }
}
