/// Global pose sensors
use num_enum::{IntoPrimitive, TryFromPrimitive};
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ///  Latitude in millionths of a degree
    Latitude = mmp_sys::CFG_NAV_LATITUDE,
    ///  Longitude in millionths of a degree
    Longitude = mmp_sys::CFG_NAV_LONGITUDE,
    ///  absolute altitude relative to WGS84
    GpsAlt = mmp_sys::CFG_NAV_GPSALT,
    ///  altitude above sea floor in meters
    Altitude = mmp_sys::CFG_NAV_ALTITUDE,
    ///  depth below sea level in meters
    Depth = mmp_sys::CFG_NAV_DEPTH,
    ///  compass bearing in degrees
    Compass = mmp_sys::CFG_NAV_COMPASS,
    ///  pitch in degrees
    Pitch = mmp_sys::CFG_NAV_PITCH,
    ///  roll in degrees
    Roll = mmp_sys::CFG_NAV_ROLL,
    ///  speed of sound in m/s
    SoundSpeed = mmp_sys::CFG_NAV_CSOUND,
    ///  additional data fields for position information
    ReplyData = mmp_sys::CFG_NAV_REPLY_DATA,
    ///  offset of compass heading to vehicle center line
    HeaddingOffset = mmp_sys::CFG_NAV_HEADOFFSET,
    ///  Enable one-way ranging on data transmissions when sychronized to externa 1PPS sources
    SyncRanging = mmp_sys::CFG_NAV_SYNCRANGING,
    ///  offset of AHRS pitch to transducer plane
    PitchOffset = mmp_sys::CFG_NAV_PITCHOFFSET,
    ///  offset of AHRS roll to transducer plane
    RollOffset = mmp_sys::CFG_NAV_ROLLOFFSET,
    ///  The type of GPS sentence, if any, that may be used to adjust the modem's system clock
    GpsSyncMsg = mmp_sys::CFG_NAV_GPSSYNCMSG,
}
