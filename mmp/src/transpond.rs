use num_enum::{IntoPrimitive, TryFromPrimitive};

/// Transponder and Ranging data/notifications
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ///  Transpond mode status
    Stat = mmp_sys::MMP_TRANSPOND_FLD_STAT,
    ///  Transpond ping received X
    PingRecvd = mmp_sys::MMP_TRANSPOND_FLD_PINGRCVD,
    ///  Receive sensitivity adjustment for channel 0
    RxAdj0 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_0,
    ///  Receive sensitivity adjustment for channel 1
    RxAdj1 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_1,
    ///  Receive sensitivity adjustment for channel 2
    RxAdj2 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_2,
    ///  Receive sensitivity adjustment for channel 3
    RxAdj3 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_3,
    ///  Receive sensitivity adjustment for channel 4
    RxAdj4 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_4,
    ///  Receive sensitivity adjustment for channel 5
    RxAdj5 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_5,
    ///  Receive sensitivity adjustment for channel 6
    RxAdj6 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_6,
    ///  Receive sensitivity adjustment for channel 7
    RxAdj7 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_7,
    ///  Receive sensitivity adjustment for channel 8
    RxAdj8 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_8,
    ///  Receive sensitivity adjustment for channel 9
    RxAdj9 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_9,
    ///  Receive sensitivity adjustment for channel 10
    RxAdj10 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_10,
    ///  Receive sensitivity adjustment for channel 11
    RxAdj11 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_11,
    ///  Receive sensitivity adjustment for channel 12
    RxAdj12 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_12,
    ///  Receive sensitivity adjustment for channel 13
    RxAdj13 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_13,
    ///  Receive sensitivity adjustment for channel 14
    RxAdj14 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_14,
    ///  Receive sensitivity adjustment for channel 15
    RxAdj15 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_15,
    ///  Receive sensitivity adjustment for channel 16
    RxAdj16 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_16,
    ///  Receive sensitivity adjustment for channel 17
    RxAdj17 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_17,
    ///  Receive sensitivity adjustment for channel 18
    RxAdj18 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_18,
    ///  Receive sensitivity adjustment for channel 19
    RxAdj19 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_19,
    ///  Receive sensitivity adjustment for channel 20
    RxAdj20 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_20,
    ///  Receive sensitivity adjustment for channel 21
    RxAdj21 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_21,
    ///  Receive sensitivity adjustment for channel 22
    RxAdj22 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_22,
    ///  Receive sensitivity adjustment for channel 23
    RxAdj23 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_23,
    ///  Receive sensitivity adjustment for channel 24
    RxAdj24 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_24,
    ///  Receive sensitivity adjustment for channel 25
    RxAdj25 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_25,
    ///  Receive sensitivity adjustment for channel 26
    RxAdj26 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_26,
    ///  Receive sensitivity adjustment for channel 27
    RxAdj27 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_27,
    ///  Receive sensitivity adjustment for channel 28
    RxAdj28 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_28,
    ///  Receive sensitivity adjustment for channel 29
    RxAdj29 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_29,
    ///  Receive sensitivity adjustment for channel 30
    RxAdj30 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_30,
    ///  Receive sensitivity adjustment for channel 31
    RxAdj31 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_31,
    ///  Receive sensitivity adjustment for channel 32
    RxAdj32 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_32,
    ///  Receive sensitivity adjustment for channel 33
    RxAdj33 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_33,
    ///  Receive sensitivity adjustment for channel 34
    RxAdj34 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_34,
    ///  Receive sensitivity adjustment for channel 35
    RxAdj35 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_35,
    ///  Receive sensitivity adjustment for channel 36
    RxAdj36 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_36,
    ///  Receive sensitivity adjustment for channel 37
    RxAdj37 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_37,
    ///  Receive sensitivity adjustment for channel 38
    RxAdj38 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_38,
    ///  Receive sensitivity adjustment for channel 39
    RxAdj39 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_39,
    ///  Receive sensitivity adjustment for channel 40
    RxAdj40 = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_40,
    ///  Set or get all receive channel sensitivity adjustments at once
    RxAdjALL = mmp_sys::MMP_TRANSPOND_FLD_CHNLRXADJ_ALL,
}
