/// Data recorder configuration
use num_enum::{IntoPrimitive, TryFromPrimitive};
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ///  recording mode for automatic recording
    RecMode = mmp_sys::CFG_RECORDER_RECMODE,
    ///  format of names for recorded audio files
    NameFormat = mmp_sys::CFG_RECORDER_NAMEFORMAT,
}
