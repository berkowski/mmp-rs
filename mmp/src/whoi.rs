/// WHOI configuration
use num_enum::{IntoPrimitive, TryFromPrimitive};
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ///  Display RXD message
    Rxd = mmp_sys::CFG_WHOI_RXD,
    ///  Display RXA message
    Rxa = mmp_sys::CFG_WHOI_RXA,
    ///  Heartbeat Cycle Timing
    Cycto = mmp_sys::CFG_WHOI_CYCTO,
    ///  Host Data Timing
    Dato = mmp_sys::CFG_WHOI_DATO,
    ///  Acoustic Data Timing
    PktTo = mmp_sys::CFG_WHOI_PKTTO,
}
