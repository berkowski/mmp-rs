/// Release functionality
use mmp_serialize::{Deserialize, Serialize};

use num_enum::{IntoPrimitive, TryFromPrimitive};

const SUBSYSTEM: crate::Subsystem = crate::Subsystem::ConfigRelease;

#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    /// Duration (seconds) to play Benthos
    /// FSK release tones
    FskReleaseDuration = mmp_sys::CFG_RELEASE_FSKRLSDUR,
    /// Lost communication count (15
    /// second increments)
    LostCommsCount = mmp_sys::CFG_RELEASE_LSTCOMMSCNT,
    /// Release code to trigger this release
    /// (valid on release models only)
    RlsCode = mmp_sys::CFG_RELEASE_RLSCODE,
    ///  Timeout count for timed releases
    TimedReelase = mmp_sys::CFG_RELEASE_TIMEDRELEASE,
    /// Type of motor used in this release;
    /// only valid for releases which can
    /// support differing motor type
    MotorType = mmp_sys::CFG_RELEASE_MOTORTYPE,
    ///  Minimun on time for Release Enable
    RlsMinTime = mmp_sys::CFG_RELEASE_RLSMINENATIME,
    /// Maximin on time for Release
    /// Enable
    RlsMaxTime = mmp_sys::CFG_RELEASE_RLSMAXENATIME,
}

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[mmp(system = "SUBSYSTEM")]
#[mmp(field = "Field::RlsCode")]
#[mmp(kind = "crate::config::Num32")]
pub struct ReleaseCode(pub u16);

#[cfg(test)]
mod ser {
    use super::*;

    #[test]
    fn release_code() {
        let expected = &[0x44, 0x02, 0x00, 0x06, 0x00, 0x00, 0x00, 23, 0x00, 0x01][..];
        let mut buf = [0u8; 64];
        let (result, _) = mmp_serialize::to_bytes(&ReleaseCode(23), &mut buf[..]).unwrap();

        assert_eq!(expected, result);
    }
}
#[cfg(test)]
mod de {
    use super::*;

    #[test]
    fn release_code() {
        let expected = ReleaseCode(23);
        let (_, result) = mmp_serialize::from_bytes(
            &[0x44, 0x02, 0x00, 0x06, 0x00, 0x00, 0x00, 23, 0x00, 0x01][..],
        )
        .unwrap();

        assert_eq!(expected, result);
    }
}
