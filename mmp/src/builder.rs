//! IMPORTANT:  THIS IS NOT READY YET
//! IT REPRESENTS WHERE I LEFT OFF WORK.  I DIDN'T LIKE THE RESULTS, BUT DON'T
//! WANT TO LOSE THEM EITHER.  SO IT'LL HANG AROUND HERE FOR A BIT
//!
//! Construct MMP messages using the builder pattern.
//!
//!
//! Using the builder pattern changes some runtime errors
//! into compile-time errors:
//!
//! - Mixing up different MMP types within the same message
//! - Trying to do something impossible (e.g. setting a GET-only field)
//! - Using the wrong data type for the given parameter
//!
//!
//! The entry point here is `MessageBuilder::with_writer` which accepts a
//! mutable reference to something that impliments the `Writer` trait.
//! Then you can set the transation ID before determining which type of message
//! you will build using one of the `set`, `get`, `notify`, or `execute` methods.
//! You can add additional parameters with the builder's `add` method.
//! Finally, you call the `finish` method to get the message as a slice of bytes.
//!
//! # Example: getting the speaker volume
//! ```
//! use mmp::{
//!    SliceWriter, to_writer,
//!    builder::{MessageBuilder, Get},
//!    deckbox::{PhonesVolume, system::Deckbox, field::PhonesVolume as PhonesVolumeField},
//! };
//!
//!
//! let mut buffer = [0u8; 64];
//! let mut writer = SliceWriter::with_slice(&mut buffer[..]);
//! let xid = 2;
//! let data = PhonesVolume {
//!     volume: 128,
//!     mute: false,
//! };
//!
//! let expected_result = [0x40, 0x02, 0x73, 0x01, 0x02, 0x01, 0x00, 0x02, 0x80, 0x00];
//!
//! let message = MessageBuilder::with_writer(&mut writer)
//!            .unwrap()
//!            .with_xid(xid)
//!            .set::<Deckbox, PhonesVolumeField, _>(&data)
//!            .unwrap()
//!            .finish();
//!
//! assert_eq!(message, expected_result);
//!```

use crate::{MessageKind, MMP_SENTINEL};

use serde::{ser::SerializeStruct, Serialize};

use serde_benthos_mmp::{to_writer, Error, Writer};

/// Get the group's "system" value
pub trait System {
    fn system() -> u8;
}

/// Get the group's "field" value
pub trait Field {
    fn field() -> u8;
}

/// Get the size of the group's data value in bytes
pub trait Data {
    fn size(&self) -> u16;
}

/// A MMP message group
///
/// MMP messages consist of a sequence of group objects which contain:
///
/// - system ID
/// - the field ID within the system
/// - the serialized size of the contained data
/// - the data itself
struct Group<'a, T: Data + Serialize> {
    id: u8,
    field: u8,
    data: &'a T,
}

impl<'a, T: Data + Serialize> Serialize for Group<'a, T> {
    fn serialize<Ser>(&self, serializer: Ser) -> Result<Ser::Ok, Ser::Error>
    where
        Ser: serde::ser::Serializer,
    {
        let mut state = serializer.serialize_struct("Group", 4)?;
        state.serialize_field("system", &self.id)?;
        state.serialize_field("field", &self.field)?;
        state.serialize_field("size", &self.data.size())?;
        state.serialize_field("value", &self.data)?;
        state.end()
    }
}

/// A trait for groups that can be used with MMP GET messages
pub trait Get<S: System, F: Field> {}

/// A trait for groups that can be used with MMP SET messages
pub trait Set<S: System, F: Field> {}

/// A trait for groups that can be used with MMP NOTIFY messages
pub trait Notify<S: System, F: Field> {}

/// Initial builder state before message type is determined
pub struct BuilderInit;
/// A builder for message type 'GET'
pub struct BuilderGet;
/// A builder for message type 'SET'
pub struct BuilderSet;
/// A builder for message type 'NOTIFY'
pub struct BuilderNotify;
/// A builder for message type 'EXECUTE'
pub struct BuilderExecute;

/// Construct MMP messages using a builder pattern
pub struct MessageBuilder<'a, B, S>
where
    B: Writer,
{
    writer: &'a mut B,
    op: core::marker::PhantomData<S>,
}

impl<'a, W> MessageBuilder<'a, W, BuilderInit>
where
    W: Writer,
{
    /// Create a MessageBuilder with the provided writer
    ///
    /// The default value for the xid of the message is 1
    pub fn with_writer(writer: &'a mut W) -> Result<Self, W::Error> {
        writer.clear();
        writer.extend(&[MMP_SENTINEL, 1, 0, 0])?;

        Ok(Self {
            writer,
            op: core::marker::PhantomData,
        })
    }

    /// Set the xid value
    pub fn with_xid(self, xid: u8) -> Self {
        self.writer.as_mut_bytes()[Self::XID_INDEX] = xid;
        self
    }
}

impl<'a, W, T> MessageBuilder<'a, W, T>
where
    W: Writer,
{
    // /// Index in byte buffer of sentinel marker
    // const SENTINEL_INDEX: usize = 0;
    /// Index in byte buffer of transaction id
    const XID_INDEX: usize = 1;
    /// Index in byte buffer of message type
    const TYPE_INDEX: usize = 2;
    /// Index in byte buffer of parameter count
    const PARAM_COUNT_INDEX: usize = 3;

    /// Update internal buffer with group data
    fn write<S, F, D>(self, data: &D) -> Result<Self, Error>
    where
        S: System,
        F: Field,
        D: Data + Serialize,
    {
        let group = Group {
            id: S::system(),
            field: F::field(),
            data,
        };

        to_writer(&group, self.writer)?;

        self.writer.as_mut_bytes()[Self::PARAM_COUNT_INDEX] += 1;
        Ok(self)
    }

    pub fn get<S, F, D>(self, data: &D) -> Result<MessageBuilder<'a, W, BuilderGet>, Error>
    where
        S: System,
        F: Field,
        D: Data + Serialize + Get<S, F>,
    {
        let writer = self.writer;

        writer.as_mut_bytes()[Self::TYPE_INDEX] = MessageKind::Get as u8;

        let self_: MessageBuilder<_, BuilderGet> = MessageBuilder {
            writer,
            op: core::marker::PhantomData,
        };

        self_.and::<S, F, _>(data)
    }

    pub fn set<S, F, D>(self, data: &D) -> Result<MessageBuilder<'a, W, BuilderSet>, Error>
    where
        S: System,
        F: Field,
        D: Data + Serialize + Set<S, F>,
    {
        let writer = self.writer;

        writer.as_mut_bytes()[Self::TYPE_INDEX] = MessageKind::Set as u8;

        let self_: MessageBuilder<_, BuilderSet> = MessageBuilder {
            writer,
            op: core::marker::PhantomData,
        };

        self_.and::<S, F, _>(data)
    }

    pub fn finish(self) -> &'a [u8] {
        self.writer.as_bytes()
    }
}

impl<'a, W> MessageBuilder<'a, W, BuilderGet>
where
    W: Writer,
{
    pub fn and<S, F, D>(self, data: &D) -> Result<Self, Error>
    where
        S: System,
        F: Field,
        D: Data + Serialize + Get<S, F>,
    {
        self.write::<S, F, _>(data)
    }
}

impl<'a, B> MessageBuilder<'a, B, BuilderSet>
where
    B: Writer,
{
    pub fn and<S, F, D>(self, data: &D) -> Result<Self, Error>
    where
        S: System,
        F: Field,
        D: Data + Serialize + Set<S, F>,
    {
        self.write::<S, F, _>(data)
    }
}

// impl<'a, B> MessageBuilder<B, Execute>
// where
//     B: Buffer + 'a,
// {
//     // pub fn execute(self, command: Command, data: &[u8]) -> Result<Self, B> {
//     //     self.write_group(command, data)
//     // }

//     pub fn into_bytes(self) -> &'a [u8] {
//         if let Some(v) = self.buf.as_bytes().get_mut(Self::TYPE_INDEX) {
//             *v = MessageKind::Execute as u8;
//         }
//         self.buf.as_bytes()
//     }
// }

mod test {
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn test_buffer_too_short() {
        let mut buffer = [0u8; 3];
        let mut writer = serde_benthos_mmp::SliceWriter::with_slice(&mut buffer[..]);
        assert!(MessageBuilder::with_writer(&mut writer).is_err());
    }

    // #[test]
    // fn mismatch_operations() {
    //     let mut buffer = [0u8; 64];
    //     assert!(MessageBuilder::with_buffer(&mut buffer)
    //         .unwrap()
    //         .get(Group::IFace(
    //             IFace::Version,
    //         ))
    //         .unwrap()
    //         .set(
    //             Group::Deckbox(Deckbox::PhonesVolume,),
    //             &[0x80, 0x00],
    //         )
    //         .is_err());
    // }

    //     #[test]
    //     fn test_fig_11() {
    //         let mut buffer = [0u8; 64];
    //         let expected_result = [0x40, 0x01, 0x67, 0x01, 0x00, 0x02, 0x00, 0x00];
    //         let message = MessageBuilder::with_buffer(&mut buffer)
    //             .unwrap()
    //             .with_xid(1)
    //             .get(Group::IFace(IFace::Version))
    //             .unwrap()
    //             .into_bytes();

    //         assert_eq!(message, expected_result);
    //     }

    #[test]
    fn test_fig_14() {
        let mut buffer = [0u8; 64];
        let mut writer = serde_benthos_mmp::SliceWriter::with_slice(&mut buffer[..]);
        let xid = 2;
        let data = crate::deckbox::PhonesVolume {
            volume: 128,
            mute: false,
        };

        let expected_result = [0x40, 0x02, 0x73, 0x01, 0x02, 0x01, 0x00, 0x02, 0x80, 0x00];
        let message = MessageBuilder::with_writer(&mut writer)
            .unwrap()
            .with_xid(xid)
            .set::<crate::deckbox::system::Deckbox, crate::deckbox::field::PhonesVolume, _>(&data)
            .unwrap()
            .finish();

        assert_eq!(message, expected_result);
    }
    //     #[test]
    //     fn test_fig_17() {
    //         let mut buffer = [0u8; 64];
    //         let expected_result = [
    //             0x40, 0x04, 0x73, 0x02, 0x02, 0x00, 0x00, 0x02, 0x3F, 0x01, 0x05, 0x09, 0x00, 0x02,
    //             0xFF, 0xE7,
    //         ];
    //         let message = MessageBuilder::with_buffer(&mut buffer)
    //             .unwrap()
    //             .with_xid(4)
    //             .set(Group::Deckbox(Deckbox::SpeakerVolume), &[0x3F, 0x01])
    //             .unwrap()
    //             .set(Group::Transpond(Transpond::RxAdj7), &[0xFF, 0xE7])
    //             .unwrap()
    //             .into_bytes();

    //         assert_eq!(message, expected_result);
    //     }
    //     #[test]
    //     fn test_fig_19() {
    //         let mut buffer = [0u8; 64];
    //         let expected_result = [0x40, 0x05, 0x78, 0x01, 0x02, 0x01, 0x00, 0x02, 0x00, 0x0A];
    //         let message = MessageBuilder::with_buffer(&mut buffer)
    //             .unwrap()
    //             .with_xid(5)
    //             .execute(Command::RngRls(RngRls::Range), &[0x00, 0x0A])
    //             .unwrap()
    //             .into_bytes();

    //         assert_eq!(message, expected_result);
    //     }
}
