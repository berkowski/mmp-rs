/// Modem functionality
use mmp_serialize::{Deserialize, Serialize};
use num_enum::{IntoPrimitive, TryFromPrimitive};

const SUBSYSTEM: crate::Subsystem = crate::Subsystem::ConfigModem;

#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ///  Acoustic data retry mode
    DataRetry = mmp_sys::CFG_MODEM_DATARETRY,
    ///  Acoustic response timeout
    AcRspTimeout = mmp_sys::CFG_MODEM_ACRSPTMOUT,
    ///  Operation mode
    OpMode = mmp_sys::CFG_MODEM_OPMODE,
    ///  Device enable line behavior
    DevEnable = mmp_sys::CFG_MODEM_DEVENABLE,
    ///  Data packet forwarding delay
    FwdDelay = mmp_sys::CFG_MODEM_FWDDELAY,
    ///  Modem local address
    LocalAddr = mmp_sys::CFG_MODEM_LOCALADDR,
    ///  Print modem data hexadecimal
    PrintHex = mmp_sys::CFG_MODEM_PRNTHEX,
    ///  Modem default remote address
    RemoteAddr = mmp_sys::CFG_MODEM_REMOTEADDR,
    ///  Receive packet type
    RxPacketType = mmp_sys::CFG_MODEM_RXPKTTYPE,
    ///  Determine whether to process or discard data with bit errors
    ShowBadData = mmp_sys::CFG_MODEM_SHOWBADDATA,
    /// Control playing of startup tones
    /// upon boot
    ModemSmartTones = mmp_sys::CFG_MODEM_STARTTONES,
    ///  Acoustic transmit bitrate of data
    TxRate = mmp_sys::CFG_MODEM_TXRATE,
    ///  Acoustic transmit power level
    TxPower = mmp_sys::CFG_MODEM_TXPOWER,
    ///  Control sending of wakeup preamble tones.
    WakeTones = mmp_sys::CFG_MODEM_WAKETONES,
    /// Direct CLAM shell to behave in
    /// "strict AT" (3rd gen style AT
    /// commands only) mode or not
    StrictAt = mmp_sys::CFG_MODEM_STRICTAT,
    /// Select the UART input mode for
    /// data transfer and data logging
    /// (single, dual)
    InputMode = mmp_sys::CFG_MODEM_INPUTMODE,
    /// Control use of SmartRetry
    /// functionality for corrupted packet
    /// transmissions when data retries are
    /// enabled
    SmartRetry = mmp_sys::CFG_MODEM_SMARTRETRY,
    ///  Layer 2 Protocol selector
    L2Protocol = mmp_sys::CFG_MODEM_L2PROTOCOL,
    /// Acoustic transmit bitrate rate of header
    HeaderRate = mmp_sys::CFG_MODEM_HEADERRATE,
    ///  Domain key that is used to create comms exclusivity groups
    DomainKey = mmp_sys::CFG_MODEM_DOMAINKEY,
    /// Determine whether to enable auto
    /// detect of header or use setting in
    /// HeaderRate parm
    AutoDetectHdr = mmp_sys::CFG_MODEM_AUTODETECTHDR,
    /// threshold adjustment for chirp
    /// detection
    ChirpThreshold = mmp_sys::CFG_MODEM_CHIRP_THRESHOLD,
    /// Sets the maximum acoustic SPL
    /// output; the @TxPower parameter
    /// range is scaled according to this
    /// parameter.
    TxAttenuation = mmp_sys::CFG_MODEM_TXATTEN,
    ///  Moves LocalAddr to another group
    AddressGroup = mmp_sys::CFG_MODEM_ADDRGROUP,
}

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[mmp(system = "SUBSYSTEM")]
#[mmp(field = "Field::LocalAddr")]
#[mmp(kind = "crate::config::Num16")]
pub struct LocalAddress(pub u8);
