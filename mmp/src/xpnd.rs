//! Transponder mode configuration
use mmp_serialize::{Deserialize, Serialize};
use num_enum::{IntoPrimitive, TryFromPrimitive};

const SUBSYSTEM: crate::Subsystem = crate::Subsystem::ConfigXpnd;

#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    /// Frequency to listen for response pings
    /// (applicable only to platforms without MultiRx capability)
    RxFreq = mmp_sys::CFG_XPND_RXFREQ,
    ///  Ping response lockout, in milliseconds
    RxLockout = mmp_sys::CFG_XPND_RXLOCKOUT,
    /// Receive pulse width (enumerated: 0 =
    /// 12.5 ms, 1 = 6.25 ms, 5 = 5 ms ... 15 = 15
    /// ms)
    RxToneDuration = mmp_sys::CFG_XPND_RXTONEDUR,
    /// Transpond turn-around time at remote
    /// node, in tenths of a millisecond
    TurnAroundTime = mmp_sys::CFG_XPND_TAT,
    /// Interrogation pulse width (enumerated: 0 =
    /// 12.5 ms, 1 = 6.25 ms, 5 = 5 ms ... 15 = 15
    /// ms)
    TxToneDur = mmp_sys::CFG_XPND_TXTONEDUR,
    /// Receive detection threshold for
    /// transponder pings, in standard deviations
    /// above mean noise level
    RxThreshold = mmp_sys::CFG_XPND_RXTHRESH,
    /// The AGC level that should be set when
    /// the avg. background noise energy in the
    /// center of the band is at 1. Lower values
    /// lower the noise floor allowing for more
    /// signal head-room; higher values raise it
    /// allowing more resolution for detecting
    /// weak signals.
    AgcReference = mmp_sys::CFG_XPND_AGCREF,
    /// Response tone sent on reception of tone
    /// set by CFG_XPND_RXFREQ after delay
    /// of CFG_XPND_TAT.
    ResponseFreq = mmp_sys::CFG_XPND_RESPFREQ,
    /// Defines the action taken on the reception
    /// of a downward HFM chirp
    LblMode = mmp_sys::CFG_XPND_LBLMODE,
    /// Transponder emulator response turnaround time in tenths of a millisecond
    EmulationTurnAroundTime = mmp_sys::CFG_XPND_XPNDEMUTAT,
    ///  Transponder emulator mode
    EumulationMode = mmp_sys::CFG_XPND_XPNDEMUMODE,
    ///  Set the HPR400 channel
    HPR400Channel = mmp_sys::CFG_XPND_HPR400CHAN,
    ///  Enable Pulse responder mode
    ResponerMode = mmp_sys::CFG_XPND_RESPONDER,
    /// Define response to be requested as a
    /// reply to at%rr command
    ChirpResponse = mmp_sys::CFG_XPND_CHIRP_RESP,
    ///  Set bandwidth for processing
    Bandwidth = mmp_sys::CFG_XPND_BANDWIDTH,
    ///  Enable logging to data logger
    LogResults = mmp_sys::CFG_XPND_LOGRESULTS,
}

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[mmp(system = "SUBSYSTEM")]
#[mmp(field = "Field::RxFreq")]
#[mmp(kind = "crate::config::Num32")]
pub struct RxFreq(u16);

impl RxFreq {
    pub fn value(&self) -> u16 {
        self.0
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[mmp(system = "SUBSYSTEM")]
#[mmp(field = "Field::ResponseFreq")]
#[mmp(kind = "crate::config::Num32")]
pub struct ResponseFreq(u16);

impl ResponseFreq {
    pub fn value(&self) -> u16 {
        self.0
    }
}

#[cfg(test)]
mod test {
    use super::*;

    mod ser {
        use super::*;

        #[test]
        fn response_freq() {
            let data = &[74, 7, 0, 6, 0, 0, 46, 224, 0, 1];

            let expected = ResponseFreq(12000);
            let (_, result) = mmp_serialize::from_bytes(data).unwrap();

            assert_eq!(expected, result);
        }
    }
}
