/// Sonar Modem configuration
use num_enum::{IntoPrimitive, TryFromPrimitive};
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ToneDetect = mmp_sys::CFG_SONAR_TONE_DETECT,
    ToneMin = mmp_sys::CFG_SONAR_TONE_MIN,
    ToneMax = mmp_sys::CFG_SONAR_TONE_MAX,
    ToneThresh = mmp_sys::CFG_SONAR_TONE_THRESH,
    /// analysis on or off
    NoiseAnalysis = mmp_sys::CFG_SONAR_NOISE_ANALYSIS,
    AgcInit = mmp_sys::CFG_SONAR_AGC_INIT,
    /// real only samples or carrier moved to complex base-band
    Frontend = mmp_sys::CFG_SONAR_FRONTEND,
    /// analysis bandwidth in Hz
    Bandwidth = mmp_sys::CFG_SONAR_BANDWIDTH,
}
