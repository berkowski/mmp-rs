use num_enum::{IntoPrimitive, TryFromPrimitive};

/// Co-processor board
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    /// Indicate power mode of coprocessor
    /// board
    CpBoard = mmp_sys::CFG_COPROC_CPBOARD,
    ///  PSK number of forward taps
    FwdTaps = mmp_sys::CFG_COPROC_FDFWDTAPS,
    /// PSK number of backward taps
    BckTaps = mmp_sys::CFG_COPROC_FDBCKTAPS,
}
