/// Transport layer routing/tagging information
use num_enum::{IntoPrimitive, TryFromPrimitive};
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ///  Global enable or disable for transport layer activity
    L4Enb = mmp_sys::CFG_TRANSPORT_L4ENB,
    ///  Enable mode for transport layer addressing information (force always on or track with InputMode)
    TpMode = mmp_sys::CFG_TRANSPORT_TPMODE,
    ///  Transport address to be applied to transmitted packets originating on UART 0
    SrcP1 = mmp_sys::CFG_TRANSPORT_SRCP1,
    ///  Transport address to be applied to transmitted packets originating on UART 1
    SrcP2 = mmp_sys::CFG_TRANSPORT_SRCP2,
    ///  Delivery destination for received packets tagged with transport address 1
    Dst1 = mmp_sys::CFG_TRANSPORT_DST1,
    ///  Delivery destination for received packets tagged with transport address 2
    Dst2 = mmp_sys::CFG_TRANSPORT_DST2,
    ///  Delivery destination for received packets tagged with transport address 3
    Dst3 = mmp_sys::CFG_TRANSPORT_DST3,
    ///  Delivery destination for received packets tagged with transport address 4
    Dst4 = mmp_sys::CFG_TRANSPORT_DST4,
}
