//! Analog Input configuration
use num_enum::{IntoPrimitive, TryFromPrimitive};
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ///  time interval for automated polling
    AinPollRate = mmp_sys::CFG_AIN_POLLRATE,
    ///  number of measurements on chan 1
    Ain1NMeasure = mmp_sys::CFG_AIN_1NMEAS,
    ///  delay in 1st reading after turn-on (ch1)
    Ain1StartDelay = mmp_sys::CFG_AIN_1STRTDLY,
    ///  delay in each additional reading (ch1)
    Ain1RepeatDelay = mmp_sys::CFG_AIN_1RPTDLY,
    ///  type of sensor on chan 1
    Ain1Type = mmp_sys::CFG_AIN_1TYPE,
    ///  number of measurements on chan 2
    Ain2NMeasure = mmp_sys::CFG_AIN_2NMEAS,
    ///  delay in 1st reading after turn-on (ch2)
    Ain2StartDelay = mmp_sys::CFG_AIN_2STRTDLY,
    ///  delay in each additional reading (ch2)
    Ain2RepeatDelay = mmp_sys::CFG_AIN_2RPTDLY,
    ///  type of sensor on chan 2
    Ain2Type = mmp_sys::CFG_AIN_2TYPE,
    ///  number of measurements on chan 3
    Ain3NMeasure = mmp_sys::CFG_AIN_3NMEAS,
    ///  delay in 1st reading after turn-on (ch3)
    Ain3StartDelay = mmp_sys::CFG_AIN_3STRTDLY,
    ///  delay in each additional reading (ch3)
    Ain3RepeatDelay = mmp_sys::CFG_AIN_3RPTDLY,
    ///  number of measurements on chan 4
    Ain4NMeasure = mmp_sys::CFG_AIN_4NMEAS,
    ///  delay in 1st reading after turn-on (ch4)
    Ain4StartDelay = mmp_sys::CFG_AIN_4STRTDLY,
    ///  delay in each additional reading (ch4)
    Ain4RepeatDelay = mmp_sys::CFG_AIN_4RPTDLY,
    ///  number of measurements on chan 5
    Ain5NMeasure = mmp_sys::CFG_AIN_5NMEAS,
    ///  delay in 1st reading after turn-on (ch5)
    Ain5StartDelay = mmp_sys::CFG_AIN_5STRTDLY,
    ///  delay in each additional reading (ch5)
    Ain5RepeatDelay = mmp_sys::CFG_AIN_5RPTDLY,
    ///  number of measurements on chan 6
    Ain6NMeasure = mmp_sys::CFG_AIN_6NMEAS,
    ///  delay in 1st reading after turn-on (ch6)
    Ain6StartDelay = mmp_sys::CFG_AIN_6STRTDLY,
    ///  delay in each additional reading (ch6)
    Ain6RepeatDelay = mmp_sys::CFG_AIN_6RPTDLY,
}
