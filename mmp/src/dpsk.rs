/// NUWC Ranging DPSK Integration section
use num_enum::{IntoPrimitive, TryFromPrimitive};
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    /// DPSK board enable - control for the
    /// feature
    Board = mmp_sys::CFG_DPSK_DPSKBOARD,
    ///  Logging of DPSK Data
    Logging = mmp_sys::CFG_DPSK_DPSKLOGGING,
    ///  DPSK Acoustic Ouput Format
    AcFormat = mmp_sys::CFG_DPSK_DPSKACFORMAT,
}
