use crate::reader::Group;

use mmp_serialize::{Deserialize, Serialize};
use num_enum::{IntoPrimitive, TryFromPrimitive};

const SUBSYSTEM: crate::Subsystem = crate::Subsystem::IFace;

/// MMP Interface configuration and control data/notifications
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    /// Status of MMP subsystem
    MmpStat = mmp_sys::MMP_IFACE_FLD_MMPSTAT,
    /// MMP error notification
    Error = mmp_sys::MMP_IFACE_FLD_ERR,
    /// Version of MMP protocol running
    Version = mmp_sys::MMP_IFACE_FLD_VERSION,
    /// MMP command (EXECUTE) result notification
    CmdResult = mmp_sys::MMP_IFACE_FLD_CMDRESULT,
    /// Block spontaneous notifications from one or
    /// more subsystems. Used with SET, provide a
    /// variable length list of subsystems to block.
    /// Returns a variable-length list of blocked
    /// subsystems. Note that responses will always be
    /// received when induced by a direct SET/CMD on
    /// an interface even if the subsystem is blocked;
    /// this only filters spontaneous notifies.
    BlockNotify = mmp_sys::MMP_IFACE_FLD_BLOCKNOTIFY,
    /// Allow spontaneous notifications from one or
    /// more subsystems. Used with SET, provide a
    /// variable-length list of subsystems to unblock.
    /// Returns a variable-length list of unblocked
    /// subsystems.
    UnblockNotify = mmp_sys::MMP_IFACE_FLD_UNBLOCKNOTIFY,
    /// DSP firmware version currently running
    DspSwVersion = mmp_sys::MMP_IFACE_FLD_DSP_SW_VERSION,
    // Privilege level on this MMP interface (requires
    // password only if elevating from current privlev)
    PrivLev = mmp_sys::MMP_IFACE_FLD_PRIVLEV,
    /// temporarily enable a feature key
    FeatureKey = mmp_sys::MMP_IFACE_FLD_FEATURE_KEY,
    ///  Obtain the assembly serial number
    UnitSerNo = mmp_sys::MMP_IFACE_FLD_UNIT_SERNO,
    /// Enable, disable, or query the status of
    /// appended notification counter (unique on a perinterface basis)
    NtfyCount = mmp_sys::MMP_IFACE_FLD_NTFY_COUNT,
    /// Enable, disable, or query the status of
    /// appended notification XOR checksum
    /// Set an AES user key for use with encrypted
    /// code images, along with an optional timeout. If
    /// 0xFFFF is provided for the timeout value, the
    /// AES key and auto-clear timeouts are cleared.
    /// An INVALID_STATE error will result if an
    /// attempt is made to install a new key over an
    /// existing one without first clearing it.
    NtfyChecksum = mmp_sys::MMP_IFACE_FLD_NTFY_CKSUM,
    /// A compatibility number used for syncing
    /// external controllers (such as ReleaseIT display)
    /// with a given range of software version. This
    /// number may vary by platform and is intended
    /// primarily for Teledyne use.
    Compatibility = mmp_sys::MMP_IFACE_FLD_COMPATIBILITY,
}

/// MMP protocol version
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[mmp(system = "SUBSYSTEM")]
#[mmp(field = "Field::Version")]
pub struct Version {
    pub major: u16,
    pub minor: u16,
}

impl<'a> core::convert::TryFrom<Group<'a>> for Version {
    type Error = ();

    /// # Example
    /// ```
    /// use mmp::reader::Group;
    /// use mmp::iface::{Version, Field};
    /// use core::convert::TryFrom;
    ///
    /// let expected = Ok(Version{
    ///    minor: 5,
    ///    major: 2,
    /// });
    ///
    /// let group = Group::IFace{
    ///    field: Field::Version,
    ///    data: &[0, 2, 0, 4, 0, 2, 0, 5]
    /// };
    ///
    /// assert_eq!(expected, Version::try_from(group));
    /// ```
    fn try_from(v: Group<'a>) -> Result<Self, Self::Error> {
        match v {
            Group::IFace {
                field: Field::Version,
                data,
            } => mmp_serialize::from_bytes(data)
                .map(|(_, v)| v)
                .map_err(|_| ()),
            _ => Err(()),
        }
    }
}

/// Block or suprious notifications
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct BlockNotify<'a> {
    pub subsys: &'a [u8],
}

impl<'a> Serialize<'a> for BlockNotify<'a> {
    fn serialize(
        &self,
        serializer: mmp_serialize::ser::Serializer<'a>,
    ) -> Result<mmp_serialize::ser::Serializer<'a>, mmp_serialize::Error> {
        serializer
            .serialize_u8(crate::Subsystem::IFace.into())
            .and_then(|s| s.serialize_u8(Field::BlockNotify.into()))
            .and_then(|s| s.start_length())
            .and_then(|s| s.serialize_u16(self.subsys.len() as u16))
            .and_then(|s| s.serialize_bytes(self.subsys))
            .and_then(|s| s.finish_length())
    }
}

impl<'de> Deserialize<'de> for BlockNotify<'de> {
    fn deserialize(
        deserializer: mmp_serialize::de::Deserializer<'de>,
    ) -> Result<(mmp_serialize::de::Deserializer<'de>, Self), mmp_serialize::Error> {
        let (deserializer, system) = deserializer.deserialize_u8()?;
        if system != SUBSYSTEM.into() {
            return Err(mmp_serialize::Error::SubsystemMismatch);
        }

        let (deserializer, field) = deserializer.deserialize_u8()?;
        if field != Field::BlockNotify.into() {
            return Err(mmp_serialize::Error::FieldMismatch);
        }

        let (deserializer, _len) = deserializer.deserialize_u16()?;
        let (deserializer, numsys) = deserializer.deserialize_u16()?;
        let (deserializer, subsys) = deserializer.deserialize_bytes(numsys as usize)?;

        Ok((deserializer, Self { subsys }))
    }
}

/// Unblock suprious notifications
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct UnblockNotify<'a> {
    pub subsys: &'a [u8],
}

impl<'a> Serialize<'a> for UnblockNotify<'a> {
    fn serialize(
        &self,
        serializer: mmp_serialize::ser::Serializer<'a>,
    ) -> Result<mmp_serialize::ser::Serializer<'a>, mmp_serialize::Error> {
        serializer
            .serialize_u8(SUBSYSTEM.into())
            .and_then(|s| s.serialize_u8(Field::UnblockNotify.into()))
            .and_then(|s| s.start_length())
            .and_then(|s| s.serialize_u16(self.subsys.len() as u16))
            .and_then(|s| s.serialize_bytes(self.subsys))
            .and_then(|s| s.finish_length())
    }
}

impl<'de> Deserialize<'de> for UnblockNotify<'de> {
    fn deserialize(
        deserializer: mmp_serialize::de::Deserializer<'de>,
    ) -> Result<(mmp_serialize::de::Deserializer<'de>, Self), mmp_serialize::Error> {
        let (deserializer, system) = deserializer.deserialize_u8()?;
        if system != SUBSYSTEM.into() {
            return Err(mmp_serialize::Error::SubsystemMismatch);
        }

        let (deserializer, field) = deserializer.deserialize_u8()?;
        if field != Field::UnblockNotify.into() {
            return Err(mmp_serialize::Error::FieldMismatch);
        }

        let (deserializer, _len) = deserializer.deserialize_u16()?;
        let (deserializer, numsys) = deserializer.deserialize_u16()?;
        let (deserializer, subsys) = deserializer.deserialize_bytes(numsys as usize)?;

        Ok((deserializer, Self { subsys }))
    }
}

/// MMP interface status returned by MmpStat parameter
#[derive(Clone, Debug, PartialEq, IntoPrimitive, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u16)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum InterfaceStatus {
    /// The MMP interface has exited
    Exited = mmp_sys::MMP_IFACE_STAT_EXITED as u16,
    /// The MMP interface has started
    Started = mmp_sys::MMP_IFACE_STAT_STARTED as u16,
}

/// MMP Status request
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[mmp(system = "SUBSYSTEM")]
#[mmp(field = "Field::MmpStat")]
pub struct MmpStat {
    pub status: InterfaceStatus,
}

impl<'a> core::convert::TryFrom<Group<'a>> for MmpStat {
    type Error = ();

    /// # Example
    /// ```
    /// use mmp::reader::Group;
    /// use mmp::iface::{MmpStat, InterfaceStatus, Field};
    /// use core::convert::TryFrom;
    ///
    /// let expected = Ok(MmpStat{
    ///   status:  InterfaceStatus::Started
    /// });
    ///
    /// let group = Group::IFace{
    ///    field: Field::MmpStat,
    ///    data: &[0, 0, 0, 2, 0, 1]
    /// };
    ///
    /// assert_eq!(expected, MmpStat::try_from(group));
    /// ```
    fn try_from(v: Group<'a>) -> Result<Self, Self::Error> {
        match v {
            Group::IFace {
                field: Field::MmpStat,
                data,
            } => mmp_serialize::from_bytes(data)
                .map(|(_, v)| v)
                .map_err(|_| ()),
            _ => Err(()),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use mmp_serialize::from_bytes;

    #[test]
    fn deserialize_mmp_stat() {
        let data = &[SUBSYSTEM.into(), Field::MmpStat.into(), 0, 2, 0x00, 0x01];

        let expected = MmpStat {
            status: InterfaceStatus::Started,
        };

        let (tail, result) = from_bytes(data).unwrap();
        assert_eq!(0, tail.len());
        assert_eq!(expected, result);
    }
}
