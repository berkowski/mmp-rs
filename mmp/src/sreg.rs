use num_enum::{IntoPrimitive, TryFromPrimitive};

/// S-Registers
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ///  DSP SW version
    Version = mmp_sys::MMP_SREG_FLD_VERSION,
    ///  Positive acknowledgements
    PosAck = mmp_sys::MMP_SREG_FLD_POSACK,
    ///  Serial port baud & configuration
    SerialBaud = mmp_sys::MMP_SREG_FLD_SERBAUD,
    ///  Acoustic baud rate
    AcousticBaud = mmp_sys::MMP_SREG_FLD_ACOUBAUD,
    ///  Transmit power
    TxPower = mmp_sys::MMP_SREG_FLD_TXPOWER,
    ///  Acoustic response timeout
    RespTimeout = mmp_sys::MMP_SREG_FLD_RESPTIMEOUT,
    ///  Packet forwarding delay
    FwdDelay = mmp_sys::MMP_SREG_FLD_FWDDELAY,
    ///  PSK coprocessor enable/status
    CoProc = mmp_sys::MMP_SREG_FLD_COPROC,
    ///  Low-power idle timeout
    LowPowerTimeout = mmp_sys::MMP_SREG_FLD_LPTIMEOUT,
    ///  Serial port flow control
    FlowControl = mmp_sys::MMP_SREG_FLD_FLOWCTRL,
    ///  Acoustic test message length
    TestMessageLen = mmp_sys::MMP_SREG_FLD_TESTMSGLEN,
    ///  Console message verbosity
    Verbosity = mmp_sys::MMP_SREG_FLD_VERBOSITY,
    ///  Default remote modem address
    RemoteAddr = mmp_sys::MMP_SREG_FLD_REMADDR,
    ///  Local modem address
    LocalAddr = mmp_sys::MMP_SREG_FLD_LOCADDR,
    ///  Receive sensitivity threshold for transpond pings
    RxThreshold = mmp_sys::MMP_SREG_FLD_RXTHRESHOLD,
    ///  Acoustic band
    Band = mmp_sys::MMP_SREG_FLD_BAND,
    ///  Transpond interrogation ping pulse width
    TxPulseWidth = mmp_sys::MMP_SREG_FLD_TXPULSEWIDTH,
    ///  Transpond received ping pulse width
    RxPulseWidth = mmp_sys::MMP_SREG_FLD_RXPULSEWIDTH,
    ///  Transponder/ranging turn-around time
    TurnAroundTime = mmp_sys::MMP_SREG_FLD_TAT,
    ///  Internal/external 1 PPS time sync mode
    PpsSyncMode = mmp_sys::MMP_SREG_FLD_PPSSYNC,
    /// Transpond ping receive frequency (only for units
    /// that can't receive multiple frequencies)
    RxFreq = mmp_sys::MMP_SREG_FLD_RXFREQ,
    ///  Transpond ping lockout time
    #[allow(clippy::upper_case_acronyms)]
    XPNDLOCKOUT = mmp_sys::MMP_SREG_FLD_XPNDLOCKOUT,
    ///  Use to SET/GET all S-registers at once
    #[allow(clippy::upper_case_acronyms)]
    ALL = mmp_sys::MMP_SREG_FLD_ALL,
}
