use crate::reader::Group as ReaderGroup;

use mmp_serialize::{Deserialize, Serialize};
use num_enum::{IntoPrimitive, TryFromPrimitive};

use core::convert::TryFrom;

const SUBSYSTEM: crate::Subsystem = crate::Subsystem::Data;

/// Data packets and other general notifcations
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ///  Data ACK from remote modem received
    Ack = mmp_sys::MMP_DATA_FLD_ACK,
    ///  Data packet from remote modem received X
    RemoteData = mmp_sys::MMP_DATA_FLD_REMOTE_DATA,
    ///  Overtemp condition on transmit X
    XmitOvertemp = mmp_sys::MMP_DATA_FLD_XMIT_OVERTEMP,
    /// Release burn-wire burn cycle active, tilt
    /// detected (unit release)
    BurnComplete = mmp_sys::MMP_DATA_FLD_BURN_COMPLETE,
    ///  Information on T/R board and RCV module
    TRRCStatus = mmp_sys::MMP_DATA_FLD_TRRC_STATUS,
    ///  Information on feature authorization keys
    FeatKeyStatus = mmp_sys::MMP_DATA_FLD_FEAT_KEY_STATUS,
    ///  Header information on packets received
    RemoteHeader = mmp_sys::MMP_DATA_FLD_REMOTE_HEADER,
    ///  Timestamp notification for a rx or tx event
    Timestamp = mmp_sys::MMP_DATA_FLD_TIMESTAMP,
    ///  Doppler speed information from tones X
    Doppler = mmp_sys::MMP_DATA_FLD_DOPPLER,
    ///  Release burn-wire cycle timed out with no tilt X
    BurnTimeout = mmp_sys::MMP_DATA_FLD_BURN_TIMEOUT,
    ///  Floating point co-processor module version X
    FpmVersion = mmp_sys::MMP_DATA_FLD_FPM_VERSION,
    /// Notification that a PSK packet was received
    /// but FPM coprocessor is not present/enabled.
    PskPktNoCoproc = mmp_sys::MMP_DATA_FLD_PSK_PKT_NO_COPROC,
    /// Notification of the modem entering or exiting
    /// low power mode
    LowPower = mmp_sys::MMP_DATA_FLD_LOW_POWER,
    /// Acoustic statistics for the packet being
    /// received
    AcStatus = mmp_sys::MMP_DATA_FLD_ACSTATS,
    /// Data packet from remote modem containing
    /// data logger data, with CRC flags
    RemoteLogBlock = mmp_sys::MMP_DATA_FLD_REMOTE_DLOGBLK,
    /// Header received from remote modem with
    /// errors; contains acoustic statistics if header
    /// decoded, or sentinel value to indicate low
    /// acquisition detected SNR
    HeaderError = mmp_sys::MMP_DATA_FLD_HEADER_ERROR,
    /// Status of navigation sources (location,
    /// heading, attitude)
    NavStatus = mmp_sys::MMP_DATA_FLD_NAV_STATUS,
    /// Updated range to a remote node determined
    /// via acoustic communication
    RangeUpdate = mmp_sys::MMP_DATA_FLD_RANGE_UPDATE,
    /// Indication of whether a floating point
    /// coprocessor that was configured to be present
    /// at boot time failed to initialize. Will always
    /// return 0 on platforms that don't support FPM
    /// coprocessors.
    FpmBootFail = mmp_sys::MMP_DATA_FLD_FPM_BOOT_FAIL,
    ///  Gives the RCV module attenuator steps and
    /// total gain
    RcvGain = mmp_sys::MMP_DATA_FLD_RCV_GAIN,
    /// Indicates activation or deactivation of the
    /// spectrum mode, along with some parameters
    SpectrumStatus = mmp_sys::MMP_DATA_FLD_SPECTRUM_STATUS,
    /// Energy levels for frequency bins calculated
    /// during spectrum mode
    SpectrumData = mmp_sys::MMP_DATA_FLD_SPECTRUM_DATA,
}

/// Type of data contained in a RemoteHeader structure.
#[derive(
    Copy, Clone, Debug, PartialEq, TryFromPrimitive, IntoPrimitive, Serialize, Deserialize,
)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[repr(u8)]
pub enum RemoteHeaderKind {
    Cmd875C = mmp_sys::L2CMD_875C,
    Unused1 = mmp_sys::L2CMD_UNUSED1,
    Unused2 = mmp_sys::L2CMD_UNUSED2,
    NuwcRng = mmp_sys::L2CMD_NUWC_RNG,
    BrgData = mmp_sys::L2CMD_BRG_DATA,
    Transport875D = mmp_sys::L2CMD_875D_TRANSPORT,
    Unused6 = mmp_sys::L2CMD_UNUSED6,
    Unused7 = mmp_sys::L2CMD_UNUSED7,
    Unused8 = mmp_sys::L2CMD_UNUSED8,
    Unused9 = mmp_sys::L2CMD_UNUSED9,
    Unused10 = mmp_sys::L2CMD_UNUSED10,
    Srq = mmp_sys::L2CMD_SRQ,
    Fhc = mmp_sys::L2CMD_FHC,
    Fhd = mmp_sys::L2CMD_FHD,
    Data = mmp_sys::L2CMD_DATA,
    Cmd875D = mmp_sys::L2CMD_875D,
}

/// Header information on packets received
#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[mmp(system = "SUBSYSTEM")]
#[mmp(field = "Field::RemoteHeader")]
pub struct RemoteHeader {
    pub kind: RemoteHeaderKind,
    /// MSB of modspec OR command type
    pub extension: u8,
    /// LSB of modspec OR command param1
    pub param1: u8,
    /// MSB of length OR command param2
    pub param2: u8,
    /// LSB of length OR command param3
    pub param3: u8,
    /// Destination address
    pub rx_addr: u8,
    /// Source address
    pub tx_addr: u8,
    /// Cyclic redundancy check
    pub crc: u8,
}

impl<'a> TryFrom<ReaderGroup<'a>> for RemoteHeader {
    type Error = ();

    /// # Example
    /// ```
    /// use mmp::reader::Group;
    /// use mmp::data::{RemoteHeader, RemoteHeaderKind, Field};
    /// use core::convert::TryFrom;
    ///
    /// let expected = Ok(RemoteHeader{
    ///    kind: RemoteHeaderKind::Cmd875C,
    ///    extension: 84,
    ///    param1: 0,
    ///    param2: 0,
    ///    param3: 0,
    ///    rx_addr: 255,
    ///    tx_addr: 0,
    ///    crc: 0,
    /// });
    ///
    /// let group = Group::Data{
    ///    field: Field::RemoteHeader,
    ///    data: &[4, 6, 0, 8, 0, 84, 0, 0, 0, 255, 0, 0]
    /// };
    ///
    /// assert_eq!(expected, RemoteHeader::try_from(group));
    /// ```
    fn try_from(v: ReaderGroup<'a>) -> Result<Self, Self::Error> {
        match v {
            ReaderGroup::Data {
                field: Field::RemoteHeader,
                data,
            } => mmp_serialize::from_bytes(data)
                .map(|(_, v)| v)
                .map_err(|_| ()),
            _ => Err(()),
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct NotAReleaseCommand;

#[repr(u8)]
enum ReleaseCommandKind {
    /// Smart Release
    Smart = 0x74,
    /// Smart Modem Burnwire Release
    Burnwire = 0x77,
}

impl TryFrom<u8> for ReleaseCommandKind {
    type Error = NotAReleaseCommand;

    fn try_from(v: u8) -> Result<Self, Self::Error> {
        match v {
            0x74 => Ok(ReleaseCommandKind::Smart),
            0x77 => Ok(ReleaseCommandKind::Burnwire),
            _ => Err(NotAReleaseCommand),
        }
    }
}

/// Release command information from RemoteHeader
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
pub enum ReleaseCommand {
    /// Smart Release command
    Smart {
        /// Intended destination
        destination: u8,
        /// Source issuing the command
        source: u8,
        /// Release code
        code: u16,
    },
    /// Smart Modem "Burnwire" command
    Burnwire {
        /// Intended destination
        destination: u8,
        /// Source issuing the command
        source: u8,
    },
}

/// Incoming release commands can trigger Notify messages of
/// type data::RemoteHeader.  The `ReleaseCommand` enum along
/// with this `try_from` impl can be used to check if the
/// received.
///
///
/// # Example
/// ```
/// use mmp::data::{ReleaseCommand, RemoteHeader, RemoteHeaderKind};
/// use core::convert::TryFrom;
///
/// let header = RemoteHeader {
///     kind: RemoteHeaderKind::Cmd875C,
///     extension: 0x74,
///     param1: 0xCD,
///     param2: 0xAB,
///     param3: 0x00,
///     rx_addr: 0xBB,
///     tx_addr: 0x10,
///     crc: 0,
/// };
/// let expected = Ok(ReleaseCommand::Smart{
///     destination: 0xBB,
///     source: 0x10,
///     code: 0xABCD,
/// });
/// assert_eq!(expected, ReleaseCommand::try_from(header));
/// ```
impl TryFrom<RemoteHeader> for ReleaseCommand {
    type Error = NotAReleaseCommand;

    fn try_from(header: RemoteHeader) -> Result<Self, Self::Error> {
        if header.kind != RemoteHeaderKind::Cmd875C {
            return Err(NotAReleaseCommand);
        }

        match ReleaseCommandKind::try_from(header.extension)? {
            ReleaseCommandKind::Smart => Ok(Self::Smart {
                destination: header.rx_addr,
                source: header.tx_addr,
                code: u16::from_be_bytes([header.param2, header.param1]),
            }),
            ReleaseCommandKind::Burnwire => Ok(Self::Burnwire {
                destination: header.rx_addr,
                source: header.tx_addr,
            }),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use mmp_serialize::{from_bytes, to_bytes};

    #[test]
    fn deserialize_remote_header() {
        let data = &[
            crate::Subsystem::Data.into(),
            Field::RemoteHeader.into(),
            0,
            8,
            0x00,
            0x74,
            0xCD,
            0xAB,
            0x00,
            0xBB,
            0x10,
            0x00,
        ];

        let expected = RemoteHeader {
            kind: RemoteHeaderKind::Cmd875C,
            extension: 0x74,
            param1: 0xCD,
            param2: 0xAB,
            param3: 0x00,
            rx_addr: 0xBB,
            tx_addr: 0x10,
            crc: 0,
        };

        let (tail, value) = from_bytes(data).unwrap();
        assert_eq!(0, tail.len());
        assert_eq!(expected, value);
    }

    #[test]
    fn serialize_remote_header() {
        let expected = &[
            crate::Subsystem::Data.into(),
            Field::RemoteHeader.into(),
            0,
            8,
            0x00,
            0x74,
            0xCD,
            0xAB,
            0x00,
            0xBB,
            0x10,
            0x00,
        ];

        let header = RemoteHeader {
            kind: RemoteHeaderKind::Cmd875C,
            extension: 0x74,
            param1: 0xCD,
            param2: 0xAB,
            param3: 0x00,
            rx_addr: 0xBB,
            tx_addr: 0x10,
            crc: 0,
        };

        let mut buf = [0u8; 16];

        let (serialized, _tail) = to_bytes(&header, &mut buf).unwrap();

        assert_eq!(expected, serialized)
    }
}
