///  Test functionality
use num_enum::{IntoPrimitive, TryFromPrimitive};
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ///  Test debug level
    DbgLevel = mmp_sys::CFG_TEST_DBGLVL,
    ///  Receive all packets (sniffer mode)
    RcvAll = mmp_sys::CFG_TEST_RCVALL,
    ///  Test response delay
    RspDelay = mmp_sys::CFG_TEST_RSPDELAY,
    ///  Test packet echo
    PktEcho = mmp_sys::CFG_TEST_PKTECHO,
    ///  Test packet size
    PktSize = mmp_sys::CFG_TEST_PKTSIZE,
    ///  Simulated variable acoustic delay, in milliseconds
    SimAcDelay = mmp_sys::CFG_TEST_SIMACDLY,
    ///  Special test mode for enabling saturation on transmit
    TxSaturate = mmp_sys::CFG_TEST_TXSATURATE,
    ///  Special test mode DAT for first or peak arrival
    Arrival = mmp_sys::CFG_TEST_ARRIVAL,
}
