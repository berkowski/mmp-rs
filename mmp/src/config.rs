//! Standard types used for many parameters
//!
//! MMP uses standard set of types for many of the "Config" subsections.
//! By the spec, this is the type listed in "Appendex E", represented by
//! `mmp_sys::mmp_cfg_paramval_t`.
//!
//! These types are used for the following subsections:
//!  - Subsystem::ConfigCoProc
//!  - Subsystem::ConfigDatalog
//!  - Subsystem::ConfigModem
//!  - Subsystem::ConfigRelease
//!  - Subsystem::ConfigSerial
//!  - Subsystem::ConfigSystem
//!  - Subsystem::ConfigTest
//!  - Subsystem::ConfigVersion
//!  - Subsystem::ConfigXpnd
//!  - Subsystem::ConfigNetwork
//!  - Subsystem::ConfigNav

use mmp_serialize::de::Deserializer;
use mmp_serialize::ser::Serializer;
use mmp_serialize::{Deserialize, Error, Serialize};
use num_enum::{IntoPrimitive, TryFromPrimitive};

/// Kind of parameter value.
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
pub enum Kind {
    /// A signed 16-bit integer
    Num16 = mmp_sys::CFG_NUM16,
    ///  A signed 32-bit integer
    Num32 = mmp_sys::CFG_NUM32,
    /// A variable length character string
    String = mmp_sys::CFG_STR,
    /// A Boolean value (0 for false, 1 for true)
    Bool = mmp_sys::CFG_BOOL,
    /// A 32-bit integer representing a fractional number with 0.0001
    /// precision.  Divide this integer value by 10000 to obtain the
    /// true fractional value it represents.
    Fixed = mmp_sys::CFG_FIXED,
    /// A 32-bit signed enumerated value.  This config parameter is re
    /// stricted to a finite set of pre-defined values dependent on
    /// which configuration field it applies to.
    Enum = mmp_sys::CFG_ENUM,
    /// A custom format with layout dependent on the configuration
    /// field it applies to.
    Custom = mmp_sys::CFG_CSTM,
}

// Private struct used for serializing/deserializing
// This struct follows the layout of mmp_sys::mmp_cfg_paramval_t, but without
// the field packing.
//
// Care must be taken to ensure the 'value_or_length' and 'buf' fields stay in sync
// for types with variable size (Strings and Custom).
// The various From<> impl's take care of setting value_or_length for String
// and Custom during serialization.  Size bounds are checked during deserialzation
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
struct Value<'a> {
    value_or_length: i32,
    places: u8,
    kind: Kind,
    buf: &'a [u8],
}

impl<'a> Serialize<'a> for Value<'a> {
    fn serialize(
        &self,
        serializer: Serializer<'a>,
    ) -> Result<Serializer<'a>, mmp_serialize::Error> {
        serializer
            .serialize_i32(self.value_or_length)
            .and_then(|s| s.serialize_u8(self.places))
            .and_then(|s| s.serialize_u8(self.kind.into()))
            .and_then(|s| s.serialize_bytes(self.buf))
    }
}

impl<'de> Deserialize<'de> for Value<'de> {
    fn deserialize(
        deserializer: Deserializer<'de>,
    ) -> Result<(Deserializer<'de>, Self), mmp_serialize::Error> {
        let (deserializer, value_or_length) = deserializer.deserialize_i32()?;
        let (deserializer, places) = deserializer.deserialize_u8()?;
        let (deserializer, kind) = deserializer.deserialize_u8()?;
        let kind = Kind::try_from_primitive(kind).map_err(|_| mmp_serialize::Error::Deserialize)?;
        let (deserializer, buf) = match kind {
            Kind::String | Kind::Custom => deserializer.deserialize_bytes(value_or_length as usize),
            _ => Ok((deserializer, &[][..])),
        }?;

        Ok((
            deserializer,
            Self {
                value_or_length,
                places,
                kind,
                buf,
            },
        ))
    }
}

#[cfg(never)]
impl<'de> Deserialize<'de> for Value<'de> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier)]
        #[allow(non_camel_case_types)]
        enum ParamField {
            value_or_length,
            places,
            kind,
            buf,
        }

        struct ParamVisitor;

        impl<'de> serde::de::Visitor<'de> for ParamVisitor {
            type Value = Value<'de>;

            fn expecting(&self, formatter: &mut core::fmt::Formatter) -> core::fmt::Result {
                formatter.write_str("struct Param")
            }

            fn visit_seq<V>(self, mut seq: V) -> Result<Self::Value, V::Error>
            where
                V: serde::de::SeqAccess<'de>,
            {
                let value_or_length = seq
                    .next_element()?
                    .ok_or_else(|| serde::de::Error::invalid_length(0, &self))?;
                let places = seq
                    .next_element()?
                    .ok_or_else(|| serde::de::Error::invalid_length(1, &self))?;
                let kind = seq
                    .next_element()?
                    .ok_or_else(|| serde::de::Error::invalid_length(2, &self))?;
                let buf: &[u8] = seq
                    .next_element()?
                    .ok_or_else(|| serde::de::Error::invalid_length(3, &self))?;

                if kind == Kind::String || kind == Kind::Custom {
                    let length = value_or_length as usize;
                    if length > buf.len() {
                        Err(serde::de::Error::custom(
                            "expected more bytes for parameter",
                        ))
                    } else {
                        Ok(Value {
                            value_or_length,
                            places,
                            kind,
                            buf: &buf[..length],
                        })
                    }
                } else {
                    Ok(Value {
                        value_or_length,
                        places,
                        kind,
                        buf,
                    })
                }
            }

            fn visit_map<V>(self, mut map: V) -> Result<Self::Value, V::Error>
            where
                V: serde::de::MapAccess<'de>,
            {
                let mut value_or_length = None;
                let mut places = None;
                let mut kind = None;
                let mut buf = None;
                while let Some(key) = map.next_key()? {
                    match key {
                        ParamField::value_or_length => {
                            if value_or_length.is_some() {
                                return Err(serde::de::Error::duplicate_field("value_or_length"));
                            }
                            value_or_length = Some(map.next_value()?);
                        }
                        ParamField::places => {
                            if places.is_some() {
                                return Err(serde::de::Error::duplicate_field("places"));
                            }
                            places = Some(map.next_value()?);
                        }
                        ParamField::kind => {
                            if kind.is_some() {
                                return Err(serde::de::Error::duplicate_field("kind"));
                            }
                            kind = Some(map.next_value()?);
                        }
                        ParamField::buf => {
                            if buf.is_some() {
                                return Err(serde::de::Error::duplicate_field("buf"));
                            }
                            buf = Some(map.next_value()?);
                        }
                    }
                }
                let value_or_length = value_or_length
                    .ok_or_else(|| serde::de::Error::missing_field("value_or_length"))?;
                let places = places.ok_or_else(|| serde::de::Error::missing_field("places"))?;
                let kind = kind.ok_or_else(|| serde::de::Error::missing_field("kind"))?;
                let buf: &[u8] = buf.ok_or_else(|| serde::de::Error::missing_field("buf"))?;
                if kind == Kind::String || kind == Kind::Custom {
                    let length = value_or_length as usize;
                    if length > buf.len() {
                        Err(serde::de::Error::custom(
                            "expected more bytes for parameter",
                        ))
                    } else {
                        Ok(Value {
                            value_or_length,
                            places,
                            kind,
                            buf: &buf[..length],
                        })
                    }
                } else {
                    Ok(Value {
                        value_or_length,
                        places,
                        kind,
                        buf,
                    })
                }
            }
        }

        const FIELDS: &[&str] = &["value_or_length", "places", "kind", "buf"];

        deserializer.deserialize_struct("Param", FIELDS, ParamVisitor)
    }
}

/// 16-bit signd integer configuration type
#[derive(Copy, Clone, Debug, Default, PartialEq)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Num16(pub i16);

impl<'a> From<&Num16> for Value<'a> {
    fn from(v: &Num16) -> Self {
        Self {
            value_or_length: v.0.into(),
            places: 0,
            kind: Kind::Num16,
            buf: &[][..],
        }
    }
}
impl From<u8> for Num16 {
    fn from(v: u8) -> Self {
        Self(v as i16)
    }
}

impl From<Num16> for u8 {
    fn from(v: Num16) -> Self {
        v.0 as Self
    }
}

impl From<i8> for Num16 {
    fn from(v: i8) -> Self {
        Self(v as i16)
    }
}

impl From<Num16> for i8 {
    fn from(v: Num16) -> Self {
        v.0 as Self
    }
}

impl From<u16> for Num16 {
    fn from(v: u16) -> Self {
        Self(v as i16)
    }
}

impl From<Num16> for u16 {
    fn from(v: Num16) -> Self {
        v.0 as Self
    }
}

impl<'a> Serialize<'a> for Num16 {
    fn serialize(
        &self,
        serializer: mmp_serialize::ser::Serializer<'a>,
    ) -> Result<mmp_serialize::ser::Serializer<'a>, mmp_serialize::Error> {
        Value::serialize(&Value::from(self), serializer)
    }
}

impl<'de> Deserialize<'de> for Num16 {
    fn deserialize(
        deserializer: mmp_serialize::de::Deserializer<'de>,
    ) -> Result<(mmp_serialize::de::Deserializer<'de>, Self), mmp_serialize::Error> {
        let (deserializer, v) = Value::deserialize(deserializer)?;
        if v.kind == Kind::Num16 {
            Ok((deserializer, Num16(v.value_or_length as i16)))
        } else {
            Err(mmp_serialize::Error::Deserialize)
        }
    }
}

/// 32-bit signed integer configuration type
#[derive(Copy, Clone, Default, Debug, PartialEq)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Num32(pub i32);

impl<'a> From<&Num32> for Value<'a> {
    fn from(v: &Num32) -> Self {
        Self {
            value_or_length: v.0,
            places: 0,
            kind: Kind::Num32,
            buf: &[][..],
        }
    }
}

impl From<u8> for Num32 {
    fn from(v: u8) -> Self {
        Self(v as i32)
    }
}

impl From<Num32> for u8 {
    fn from(v: Num32) -> Self {
        v.0 as Self
    }
}

impl From<i8> for Num32 {
    fn from(v: i8) -> Self {
        Self(v as i32)
    }
}

impl From<Num32> for i8 {
    fn from(v: Num32) -> Self {
        v.0 as Self
    }
}

impl From<u16> for Num32 {
    fn from(v: u16) -> Self {
        Self(v as i32)
    }
}

impl From<Num32> for u16 {
    fn from(v: Num32) -> Self {
        v.0 as Self
    }
}

impl From<i16> for Num32 {
    fn from(v: i16) -> Self {
        Self(v as i32)
    }
}

impl From<Num32> for i16 {
    fn from(v: Num32) -> Self {
        v.0 as Self
    }
}

impl From<u32> for Num32 {
    fn from(v: u32) -> Self {
        Self(v as i32)
    }
}

impl From<Num32> for u32 {
    fn from(v: Num32) -> Self {
        v.0 as Self
    }
}

impl<'a> Serialize<'a> for Num32 {
    fn serialize(
        &self,
        serializer: mmp_serialize::ser::Serializer<'a>,
    ) -> Result<mmp_serialize::ser::Serializer<'a>, mmp_serialize::Error> {
        Value::serialize(&Value::from(self), serializer)
    }
}

impl<'de> Deserialize<'de> for Num32 {
    fn deserialize(
        deserializer: mmp_serialize::de::Deserializer<'de>,
    ) -> Result<(mmp_serialize::de::Deserializer<'de>, Self), mmp_serialize::Error> {
        let (deserializer, v) = Value::deserialize(deserializer)?;
        if v.kind == Kind::Num32 {
            if v.places == 0 {
                Ok((deserializer, Num32(v.value_or_length)))
            } else {
                Err(mmp_serialize::Error::Deserialize)
                //Err(serde::de::Error::custom("non-zero places value for Num32"))
            }
        } else {
            Err(mmp_serialize::Error::Deserialize)
            //Err(serde::de::Error::custom("not a Num32 value"))
        }
    }
}

/// 32-bit floating-point configuration type
///
/// This tuple type has two parts:
///
/// 0: a 32-bit signed value
/// 1: a number of decimal places to shift
///
///
/// So, for example, the value 3.1415 would be represented as
/// Float32Value(31415, 4)
#[derive(Clone, Copy, Default, Debug, PartialEq)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Float32(pub i32, pub u8);

impl<'a> From<&Float32> for Value<'a> {
    fn from(v: &Float32) -> Self {
        Self {
            value_or_length: v.0,
            places: v.1,
            kind: Kind::Num32,
            buf: &[][..],
        }
    }
}

impl<'a> Serialize<'a> for Float32 {
    fn serialize(
        &self,
        serializer: mmp_serialize::ser::Serializer<'a>,
    ) -> Result<mmp_serialize::ser::Serializer<'a>, mmp_serialize::Error> {
        Value::serialize(&Value::from(self), serializer)
    }
}

impl<'de> Deserialize<'de> for Float32 {
    fn deserialize(
        deserializer: mmp_serialize::de::Deserializer<'de>,
    ) -> Result<(mmp_serialize::de::Deserializer<'de>, Self), mmp_serialize::Error> {
        let (deserializer, v) = Value::deserialize(deserializer)?;
        if v.kind == Kind::Num32 {
            Ok((deserializer, Float32(v.value_or_length, v.places)))
        } else {
            Err(mmp_serialize::Error::Deserialize)
            //Err(serde::de::Error::custom("not a Float32 value"))
        }
    }
}

/// A string configuration variable
#[derive(Clone, Copy, Debug, Default, PartialEq)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct String<'a>(pub &'a str);

impl<'a> From<&String<'a>> for Value<'a> {
    fn from(v: &String<'a>) -> Self {
        let bytes = v.0.as_bytes();
        Self {
            value_or_length: bytes.len() as i32,
            places: 0,
            kind: Kind::String,
            buf: bytes,
        }
    }
}

impl<'a> Serialize<'a> for String<'a> {
    fn serialize(
        &self,
        serializer: mmp_serialize::ser::Serializer<'a>,
    ) -> Result<mmp_serialize::ser::Serializer<'a>, mmp_serialize::Error> {
        Value::serialize(&Value::from(self), serializer)
    }
}

impl<'de> Deserialize<'de> for String<'de> {
    fn deserialize(
        deserializer: mmp_serialize::de::Deserializer<'de>,
    ) -> Result<(mmp_serialize::de::Deserializer<'de>, Self), mmp_serialize::Error> {
        let (deserializer, v) = Value::deserialize(deserializer)?;
        if v.kind == Kind::String {
            core::str::from_utf8(v.buf)
                .map(|s| (deserializer, String(s)))
                .map_err(|_| mmp_serialize::Error::Deserialize)
            //.map_err(|_| serde::de::Error::custom("not a utf8 string"))
        } else {
            Err(mmp_serialize::Error::Deserialize)
            //Err(serde::de::Error::custom("not a String value"))
        }
    }
}

/// A custom configuration variable represented as a sequence of bytes
#[derive(Clone, Copy, Debug, Default, PartialEq)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Custom<'a>(pub &'a [u8]);

impl<'a> From<&Custom<'a>> for Value<'a> {
    fn from(v: &Custom<'a>) -> Self {
        Self {
            value_or_length: v.0.len() as i32,
            places: 0,
            kind: Kind::Custom,
            buf: v.0,
        }
    }
}

impl<'a> Serialize<'a> for Custom<'a> {
    fn serialize(
        &self,
        serializer: mmp_serialize::ser::Serializer<'a>,
    ) -> Result<mmp_serialize::ser::Serializer<'a>, mmp_serialize::Error> {
        Value::serialize(&Value::from(self), serializer)
    }
}

impl<'de> Deserialize<'de> for Custom<'de> {
    fn deserialize(
        deserializer: mmp_serialize::de::Deserializer<'de>,
    ) -> Result<(mmp_serialize::de::Deserializer<'de>, Self), mmp_serialize::Error> {
        let (deserializer, v) = Value::deserialize(deserializer)?;
        if v.kind == Kind::Custom {
            Ok((deserializer, Custom(v.buf)))
        } else {
            Err(mmp_serialize::Error::Deserialize)
            //Err(serde::de::Error::custom("not a Custom value"))
        }
    }
}

/// A 32-bit integer representing a fractional number with 0.0001 precision
///
/// Divide this number by 10000 to obtain the true fractional value it represents
#[derive(Clone, Copy, Debug, Default, PartialEq)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Fixed(pub i32);

impl<'a> From<&Fixed> for Value<'a> {
    fn from(v: &Fixed) -> Self {
        Self {
            value_or_length: v.0,
            places: 0,
            kind: Kind::Fixed,
            buf: &[][..],
        }
    }
}

impl<'a> Serialize<'a> for Fixed {
    fn serialize(
        &self,
        serializer: mmp_serialize::ser::Serializer<'a>,
    ) -> Result<mmp_serialize::ser::Serializer<'a>, mmp_serialize::Error> {
        Value::serialize(&Value::from(self), serializer)
    }
}

impl<'de> Deserialize<'de> for Fixed {
    fn deserialize(
        deserializer: mmp_serialize::de::Deserializer<'de>,
    ) -> Result<(mmp_serialize::de::Deserializer<'de>, Self), mmp_serialize::Error> {
        let (deserializer, v) = Value::deserialize(deserializer)?;
        if v.kind == Kind::Fixed {
            Ok((deserializer, Fixed(v.value_or_length)))
        } else {
            Err(mmp_serialize::Error::Deserialize)
            //Err(serde::de::Error::custom("not a Fixed value"))
        }
    }
}

/// A boolean configuration variable
#[derive(Clone, Copy, Debug, Default, PartialEq)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Bool(pub bool);

impl<'a> From<&Bool> for Value<'a> {
    fn from(v: &Bool) -> Self {
        Self {
            value_or_length: v.0 as i32,
            places: 0,
            kind: Kind::Bool,
            buf: &[][..],
        }
    }
}

impl<'a> Serialize<'a> for Bool {
    fn serialize(
        &self,
        serializer: mmp_serialize::ser::Serializer<'a>,
    ) -> Result<mmp_serialize::ser::Serializer<'a>, mmp_serialize::Error> {
        Value::serialize(&Value::from(self), serializer)
    }
}

impl<'de> Deserialize<'de> for Bool {
    fn deserialize(
        deserializer: mmp_serialize::de::Deserializer<'de>,
    ) -> Result<(mmp_serialize::de::Deserializer<'de>, Self), mmp_serialize::Error> {
        let (deserializer, v) = Value::deserialize(deserializer)?;
        if v.kind == Kind::Bool {
            Ok((deserializer, Bool(v.value_or_length != 0)))
        } else {
            Err(mmp_serialize::Error::Deserialize)
            //Err(serde::de::Error::custom("not a Bool value"))
        }
    }
}

/// A 32-bit signed enumerated value.
///
/// This config parameter is restricted to a finite set of pre-defined values
/// dependent on which configuration field it applies to.
#[derive(Clone, Copy, Debug, Default, PartialEq)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Enum(pub i32);

impl<'a> From<&Enum> for Value<'a> {
    fn from(v: &Enum) -> Self {
        Self {
            value_or_length: v.0 as i32,
            places: 0,
            kind: Kind::Enum,
            buf: &[][..],
        }
    }
}

impl<'a> Serialize<'a> for Enum {
    fn serialize(
        &self,
        serializer: mmp_serialize::ser::Serializer<'a>,
    ) -> Result<mmp_serialize::ser::Serializer<'a>, mmp_serialize::Error> {
        Value::serialize(&Value::from(self), serializer)
    }
}

impl<'de> Deserialize<'de> for Enum {
    fn deserialize(
        deserializer: mmp_serialize::de::Deserializer<'de>,
    ) -> Result<(mmp_serialize::de::Deserializer<'de>, Self), mmp_serialize::Error> {
        let (deserializer, v) = Value::deserialize(deserializer)?;
        if v.kind == Kind::Enum {
            Ok((deserializer, Enum(v.value_or_length)))
        } else {
            Err(mmp_serialize::Error::Deserialize)
            //Err(serde::de::Error::custom("not a Enum value"))
        }
    }
}

/// A value of a configuration parameter
///
/// This is the the standard 'value' type for most configuration parameters
/// covered by sections:
///
///  - Subsystem::ConfigCoProc_SECT_COPROC::*,
///  - Subsystem::ConfigDatalog
///  - Subsystem::ConfigModem
///  - Subsystem::ConfigRelease
///  - Subsystem::ConfigSerial
///  - Subsystem::ConfigSystem
///  - Subsystem::ConfigTest
///  - Subsystem::ConfigVersion
///  - Subsystem::ConfigXpnd
///  - Subsystem::ConfigNetwork
///  - Subsystem::ConfigNav
#[derive(Clone, Copy, PartialEq, Debug)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
//#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum ConfigValue<'a> {
    /// A signed 16-bit integer
    Num16(Num16),
    // /// A signed 32-bit integer
    Num32(Num32),
    /// A rational number:  Numerator / Denominator
    Float32(Float32),
    /// A string
    String(String<'a>),
    /// A boolean value
    Bool(Bool),
    /// A 32-bit integer representing a fractional number with 0.0001
    /// precision.  Divide this integer value by 10000 to obtain the
    /// true fractional value it represents.
    Fixed(Fixed),
    /// A 32-bit signed enumerated value.  This config parameter is re
    /// stricted to a finite set of pre-defined values dependent on
    /// which configuration field it applies to.
    Enum(Enum),
    /// A custom format with layout dependent on the configuration
    /// field it applies to.
    Custom(Custom<'a>),
}

impl<'a> Serialize<'a> for ConfigValue<'a> {
    fn serialize(&self, serializer: Serializer<'a>) -> Result<Serializer<'a>, Error> {
        match self {
            ConfigValue::Num16(v) => v.serialize(serializer),
            ConfigValue::Num32(v) => v.serialize(serializer),
            ConfigValue::Float32(v) => v.serialize(serializer),
            ConfigValue::String(v) => v.serialize(serializer),
            ConfigValue::Bool(v) => v.serialize(serializer),
            ConfigValue::Fixed(v) => v.serialize(serializer),
            ConfigValue::Enum(v) => v.serialize(serializer),
            ConfigValue::Custom(v) => v.serialize(serializer),
        }
    }
}

impl<'de> Deserialize<'de> for ConfigValue<'de> {
    fn deserialize(deserializer: Deserializer<'de>) -> Result<(Deserializer<'de>, Self), Error> {
        let (deserializer, value): (_, Value<'de>) = Deserialize::deserialize(deserializer)?;
        let value = match value.kind {
            Kind::Num16 => ConfigValue::Num16(Num16(value.value_or_length as i16)),
            Kind::Num32 => {
                if value.places == 0 {
                    ConfigValue::Num32(Num32(value.value_or_length))
                } else {
                    ConfigValue::Float32(Float32(value.value_or_length, value.places))
                }
            }
            Kind::Enum => ConfigValue::Enum(Enum(value.value_or_length)),
            Kind::Bool => ConfigValue::Bool(Bool(value.value_or_length != 0)),
            Kind::Fixed => ConfigValue::Fixed(Fixed(value.value_or_length)),
            Kind::String => {
                ConfigValue::String(String(unsafe { core::str::from_utf8_unchecked(value.buf) }))
            }
            Kind::Custom => ConfigValue::Custom(Custom(value.buf)),
        };

        Ok((deserializer, value))
    }
}

#[cfg(test)]
mod de {
    use super::*;
    use mmp_serialize::from_bytes;

    #[test]
    fn deserialize_string_param_value() {
        let value = "some expected string";
        let len = value.as_bytes().len();
        let expected = String(value);

        let mut data = [0u8; 64];
        data[0..4].copy_from_slice(&(value.len() as i32).to_be_bytes());
        data[4] = 0;
        data[5] = Kind::String.into();
        data[6..6 + len].copy_from_slice(value.as_bytes());

        let (_, result) = from_bytes(&data).unwrap();

        assert_eq!(expected, result);
    }

    #[test]
    fn deserialize_num16_param_value() {
        let value = 1987i16;
        let expected = Num16(value);

        let mut data = [0u8; 64];
        data[0..4].copy_from_slice(&(value as i32).to_be_bytes());
        data[4] = 0;
        data[5] = Kind::Num16.into();

        let (_, result) = from_bytes(&data).unwrap();

        assert_eq!(expected, result);
    }
}

#[cfg(test)]
mod ser {
    use super::*;
    use mmp_serialize::to_bytes;
    #[test]
    fn serialize_string_param_value() {
        let str_value = "some expected string";
        let str_len = str_value.as_bytes().len();
        let value = String(str_value);

        let mut buf = [0u8; 32];
        let (result, _) = to_bytes(&value, &mut buf).unwrap();

        let mut expected = [0u8; 32];
        expected[0..4].copy_from_slice(&(str_value.len() as i32).to_be_bytes());
        expected[4] = 0;
        expected[5] = Kind::String.into();
        expected[6..6 + str_len].copy_from_slice(str_value.as_bytes());

        assert_eq!(&expected[..6 + str_len], result);
    }
}
