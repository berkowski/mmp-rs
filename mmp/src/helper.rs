//! Helper functions for (de)serialization

/// Serialize a boolean as an unsigned 16-bit integer
pub mod bool_u16 {
    use serde::{Deserialize, Deserializer, Serializer};
    pub fn serialize<S>(v: &bool, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_u16(*v as u16)
    }
    pub fn deserialize<'de, D>(deserializer: D) -> Result<bool, D::Error>
    where
        D: Deserializer<'de>,
    {
        u16::deserialize(deserializer).map(|u| u != 0)
    }
}

pub mod timeout_x10_u16 {
    use core::time::Duration;
    use serde::{Deserialize, Deserializer, Serializer};
    pub fn serialize<S>(v: &Duration, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match v.as_secs() as u32 * 10 + v.subsec_millis() / 100 {
            timeout if timeout < u16::MAX as u32 => serializer.serialize_u16(timeout as u16),
            _ => serializer.serialize_u16(u16::MAX),
        }
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Duration, D::Error>
    where
        D: Deserializer<'de>,
    {
        u16::deserialize(deserializer).map(|tenths| Duration::from_millis((tenths as u64) * 100))
    }
}
