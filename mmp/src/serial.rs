/// UART Parameters
use num_enum::{IntoPrimitive, TryFromPrimitive};
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ///  Port 1 baud rate
    P1Baud = mmp_sys::CFG_SERIAL_P1BAUD,
    ///  Port 1 echo (full/half duplex)
    P1Echo = mmp_sys::CFG_SERIAL_P1ECHOCHAR,
    ///  Port 1 flow control
    P1FlowCtl = mmp_sys::CFG_SERIAL_P1FLOWCTL,
    ///  Port 1 idle polarity
    P1IdlePol = mmp_sys::CFG_SERIAL_P1IDLEPOL,
    ///  Port 1 comms protocol
    P1Protocol = mmp_sys::CFG_SERIAL_P1PROTOCOL,
    ///  Port 1 strip bit 7
    P1StripB7 = mmp_sys::CFG_SERIAL_P1STRIPB7,
    ///  Port 1 not counted towards activity for sleep
    P1NoSleep = mmp_sys::CFG_SERIAL_P1NOSLEEP,
    ///  Port 2 baud rate
    P2Baud = mmp_sys::CFG_SERIAL_P2BAUD,
    ///  Port 2 echo (full/half duplex)
    P2Echo = mmp_sys::CFG_SERIAL_P2ECHOCHAR,
    ///  Port 2 flow control
    P2FlowCtl = mmp_sys::CFG_SERIAL_P2FLOWCTL,
    ///  Port 2 idle polarity
    P2IdlePol = mmp_sys::CFG_SERIAL_P2IDLEPOL,
    ///  Port 2 strip bit 7
    P2StripB7 = mmp_sys::CFG_SERIAL_P2STRIPB7,
    ///  Port 2 not counted towards activity for sleep
    P2NoSleep = mmp_sys::CFG_SERIAL_P2NOSLEEP,
    ///  Port 1 cooking mode
    P1Mode = mmp_sys::CFG_SERIAL_P1MODE,
    ///  Port 2 cooking mode
    P2Mode = mmp_sys::CFG_SERIAL_P2MODE,
    ///  Port 2 on RS232 or CMOS
    P2Protocol = mmp_sys::CFG_SERIAL_P2PROTOCOL,
    ///  allow RTS lines to be off on lowpower, follow HW flow control or remain on
    LowPowerFlowCtl = mmp_sys::CFG_SERIAL_LPFLOWCTL,
    ///  control prompt display level on Port 1
    P1Prompt = mmp_sys::CFG_SERIAL_P1PROMPT,
    ///  control prompt display level on Port 2
    P2Prompt = mmp_sys::CFG_SERIAL_P2PROMPT,
}
