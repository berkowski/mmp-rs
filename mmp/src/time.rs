use num_enum::{IntoPrimitive, TryFromPrimitive};

/// Time and date-related data/notifications
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    /// (currently unsupported) Arrival of 1 PPS
    /// synchronization pulse X
    PpsPulse = mmp_sys::MMP_TIME_FLD_1PPS_PULSE,
    ///  The time and date on the modem's clock
    TimeDate = mmp_sys::MMP_TIME_FLD_TIMEDATE,
    /// (currently unsupported) Parameters governing
    /// Daylight Savings Time adjustments of local time
    Dstparams = mmp_sys::MMP_TIME_FLD_DSTPARMS,
    /// Battery level on modem's clock
    RtcBatt = mmp_sys::MMP_TIME_FLD_RTCBATT,
    /// Synchronization status of the modem to an
    /// accurate 1PPS reference (internal or external)
    /// and timestamping information (e.g., $GPZDA
    /// messages)
    SyncInfo = mmp_sys::MMP_TIME_FLD_SYNCINFO,
}
