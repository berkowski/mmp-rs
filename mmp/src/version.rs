use num_enum::{IntoPrimitive, TryFromPrimitive};
///  Version Information
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ///  Software application name string
    SwAppName = mmp_sys::CFG_VERSION_SWAPPNAME,
    ///  Software version string
    SwVersion = mmp_sys::CFG_VERSION_SWVERSION,
    ///  Configuration database version string
    DbVersion = mmp_sys::CFG_VERSION_DBVERSION,
}
