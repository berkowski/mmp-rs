use crate::reader::Group;

use mmp_serialize::{Deserialize, Serialize};
use num_enum::{IntoPrimitive, TryFromPrimitive};

const SUBSYSTEM: crate::Subsystem = crate::Subsystem::Deckbox;

/// Deckbox specific data/notifications
#[derive(Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Field {
    ///  Speaker volume
    SpeakerVolume = mmp_sys::MMP_DECKBOX_FLD_SPKRVOL,
    ///  Headphones volume
    PhonesVolume = mmp_sys::MMP_DECKBOX_FLD_PHONESVOL,
    ///  Power source information
    PowerSense = mmp_sys::MMP_DECKBOX_FLD_PWRSENSE,
    /// Internal battery level (rough percentage); a
    /// "critical" battery level will trigger a spontaneous
    /// MMP notification
    BatteryLevel = mmp_sys::MMP_DECKBOX_FLD_BATTLEV,
    ///  Internal battery voltage
    BatteryVoltage = mmp_sys::MMP_DECKBOX_FLD_BATTVOLT,
    ///  Deck box model
    Model = mmp_sys::MMP_DECKBOX_FLD_MODEL,
    /// GPS pass-through mode on display module (for
    /// tunneling external GPS time sync data through to
    /// modem - disables most display functions, only
    /// supported on some hardware configurations)
    GpsRelay = mmp_sys::MMP_DECKBOX_FLD_GPSRELAY,
}

#[derive(Copy, Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[mmp(system = "SUBSYSTEM")]
#[mmp(field = "Field::SpeakerVolume")]
pub struct SpeakerVolume {
    pub volume: u8,
    pub mute: bool,
}
impl<'a> core::convert::TryFrom<Group<'a>> for SpeakerVolume {
    type Error = ();

    fn try_from(v: Group<'a>) -> Result<Self, Self::Error> {
        match v {
            Group::Deckbox {
                field: Field::SpeakerVolume,
                data,
            } => mmp_serialize::from_bytes(data)
                .map(|(_, v)| v)
                .map_err(|_| ()),
            _ => Err(()),
        }
    }
}

#[derive(Copy, Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[mmp(system = "SUBSYSTEM")]
#[mmp(field = "Field::PhonesVolume")]
pub struct PhonesVolume {
    pub volume: u8,
    pub mute: bool,
}

impl<'a> core::convert::TryFrom<Group<'a>> for PhonesVolume {
    type Error = ();

    fn try_from(v: Group<'a>) -> Result<Self, Self::Error> {
        match v {
            Group::Deckbox {
                field: Field::PhonesVolume,
                data,
            } => mmp_serialize::from_bytes(data)
                .map(|(_, v)| v)
                .map_err(|_| ()),
            _ => Err(()),
        }
    }
}
/// Deckbox power source
#[derive(
    Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive, Serialize, Deserialize,
)]
#[repr(u16)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum PowerSenseDevice {
    /// Using internal batteries
    Batteries = mmp_sys::MMP_DECKBOX_PWRSENSE_DEVICE_BATT as u16,
    /// Using external AC power
    AC = mmp_sys::MMP_DECKBOX_PWRSENSE_DEVICE_AC as u16,
    /// Using external DC power
    AuxDC = mmp_sys::MMP_DECKBOX_PWRSENSE_DEVICE_AUXDC as u16,
}

/// Deckbox charge
#[derive(
    Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive, Serialize, Deserialize,
)]
#[repr(u8)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum PowerSenseCharge {
    /// Less than 80%
    LessThan80Pct = mmp_sys::MMP_DECKBOX_PWRSENSE_CHARGE_LT80,
    /// At least 80%
    AtLeast80Pct = mmp_sys::MMP_DECKBOX_PWRSENSE_CHARGE_GE80,
    /// Full
    Full = mmp_sys::MMP_DECKBOX_PWRSENSE_CHARGE_FULL,
}

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[mmp(system = "SUBSYSTEM")]
#[mmp(field = "Field::PowerSense")]
pub struct PowerSense {
    /// Source of deckbox power
    pub device: PowerSenseDevice,
    /// Deckbox power source charge level
    pub charge: PowerSenseCharge,
    /// Deckbox charging
    pub charging: bool,
}

/// Battery charge
#[derive(
    Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive, Serialize, Deserialize,
)]
#[repr(u16)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum BatteryLevelCharge {
    /// Empty
    Pct0 = mmp_sys::MMP_DECKBOX_BATTLEV_CHARGE_0 as u16,
    /// 20% Remaining
    Pct20 = mmp_sys::MMP_DECKBOX_BATTLEV_CHARGE_20 as u16,
    /// 40% Remaining
    Pct40 = mmp_sys::MMP_DECKBOX_BATTLEV_CHARGE_40 as u16,
    /// 60% Remaining
    Pct60 = mmp_sys::MMP_DECKBOX_BATTLEV_CHARGE_60 as u16,
    /// 80% Remaining
    Pct80 = mmp_sys::MMP_DECKBOX_BATTLEV_CHARGE_80 as u16,
    /// 100% Remaining
    Pct100 = mmp_sys::MMP_DECKBOX_BATTLEV_CHARGE_100 as u16,
    /// Critically low, auto shutdown imminent
    Critical = mmp_sys::MMP_DECKBOX_BATTLEV_CHARGE_CRITICAL as u16,
}

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[mmp(system = "SUBSYSTEM")]
#[mmp(field = "Field::BatteryLevel")]
pub struct BatteryLevel {
    /// charge level
    pub charge: BatteryLevelCharge,
}

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[mmp(system = "SUBSYSTEM")]
#[mmp(field = "Field::BatteryVoltage")]
pub struct BatteryVoltage {
    /// Internal battery voltage in 1/100 volt units
    pub volts_x_100: u16,
}

/// Deckbox model
#[derive(
    Copy, Clone, Debug, PartialEq, Eq, IntoPrimitive, TryFromPrimitive, Serialize, Deserialize,
)]
#[repr(u16)]
#[allow(non_camel_case_types)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum DeckboxModel {
    /// Unknown deck box model
    Unknown = mmp_sys::MMP_DECKBOX_MODEL_UNKNOWN as u16,
    /// UDB-9000-M fully featured
    UDB_9000_M = mmp_sys::MMP_DECKBOX_MODEL_MODEM as u16,
    /// UDB-9000-A Acoustic releases only
    UDB_9000_A = mmp_sys::MMP_DECKBOX_MODEL_RELEASE as u16,
    /// UDB-9000-LE Special
    UDB_9000_L3 = mmp_sys::MMP_DECKBOX_MODEL_MODEM_L3 as u16,
    /// UDB-9000-SW special
    UDB_9000_SW = mmp_sys::MMP_DECKBOX_MODEL_MODEM_SW as u16,
    /// UDB-9000-E export model (no multi-frequency transpond receive)
    UDB_9000_E = mmp_sys::MMP_DECKBOX_MODEL_MODEM_EXPORT as u16,
    /// UDB-9400-BN modem-enabled with BONITO Open Network Interface to Off-board systems (BONITO)
    UDB_9400_BN = mmp_sys::MMP_DECKBOX_MODEL_MODEM_BONITO as u16,
    /// UDB-9000-DR modem-enabled with data recorder
    UDP_9000_DR = mmp_sys::MMP_DECKBOX_MODEL_MODEM_DR as u16,
    /// UDB-9000-NR modem-enabled with NUWC ranging
    UDP_9000_NR = mmp_sys::MMP_DECKBOX_MODEL_MODEM_NR as u16,
    /// UDB-9000-BN modem enabled with Benthonet networking
    UDP_9000_BN = mmp_sys::MMP_DECKBOX_MODEL_MODEM_BN as u16,
}

/// Deckbox model
#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[mmp(system = "SUBSYSTEM")]
#[mmp(field = "Field::Model")]
pub struct Model {
    pub model: DeckboxModel,
}

/// Gps relay state
#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[mmp(system = "SUBSYSTEM")]
#[mmp(field = "Field::GpsRelay")]
pub struct GpsRelay {
    pub enabled: bool,
}
